<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->text('post_title');
            $table->text('post_slug');
            $table->longText('post_content')->nullable();
            $table->string('featured_image')->nullable();
            $table->boolean('hide_featured_image')->default(0);
            $table->boolean('is_sticky')->default(0);
            $table->enum('post_status',['publish','draft','pending','trash'])->default('draft');
            $table->enum('post_type',['post','page','attachment'])->default('post');
            $table->enum('comment_status',['open','close'])->default('close');
            $table->text('post_permalink');
            $table->bigInteger('post_like')->default(0);
            $table->bigInteger('post_dislike')->default(0);
            $table->bigInteger('post_thank')->default(0);
            $table->text('post_short_title')->nullable();
            $table->bigInteger('post_view')->default(0);
            $table->bigInteger('subject_id')->unsigned()->nullable();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('set null');
            $table->bigInteger('chapter_id')->unsigned()->nullable();
            $table->foreign('chapter_id')->references('id')->on('chapters')->onDelete('set null');
            $table->bigInteger('created_by')->nullable()->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->nullable()->unsigned();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');

            $table->string('seo_image')->nullable();
            $table->text('meta_keyword')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('post_faq')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
