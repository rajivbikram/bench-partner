<?php

namespace Database\Seeders;

use App\Models\Chapter;
use Illuminate\Database\Seeder;

class ChapterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chapters = [
            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>1,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>1,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>2,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>2,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>3,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>3,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>4,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>4,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>5,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>5,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>6,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>6,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>7,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>7,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>8,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>8,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>9,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>9,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>10,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>10,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>11,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>11,'created_by'=>1],

            ['name'=>'Chapter One','slug'=>'chapter-one','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Two','slug'=>'chapter-two','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Three','slug'=>'chapter-three','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Four','slug'=>'chapter-four','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Five','slug'=>'chapter-five','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Six','slug'=>'chapter-six','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Seven','slug'=>'chapter-seven','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Eight','slug'=>'chapter-eight','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Nine','slug'=>'chapter-nine','subject_id'=>12,'created_by'=>1],
            ['name'=>'Chapter Ten','slug'=>'chapter-ten','subject_id'=>12,'created_by'=>1],
        ];
        foreach($chapters as $chapter){

        	Chapter::create($chapter);
        }
    }
}