<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Model\user\setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = setting::create([
        	'blogName' => 'Blog Name',
        	'blogInfo' => 'write a blog information here.',
            'copyRightText' => 'All Right Reserve. '
        ]);
    }
}