<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            ['name'=>'Principle of Management','slug'=>'principle-of-management','status'=>'active','color'=>'#0556f3','created_by' => 1],
            ['name'=>'Operation Management','slug'=>'operation-management','status'=>'active','color'=>'#f44336','created_by' => 1],
            ['name'=>'Microeconomics','slug'=>'microeconomics','status'=>'active','color'=>'#8bc34a','created_by' => 1],
            ['name'=>'Macroeconomics','slug'=>'macroeconomics','status'=>'active','color'=>'#25476a','created_by' => 1],
            ['name'=>'Marketing','slug'=>'marketing','status'=>'active','color'=>'#ffb300','created_by' => 1],
            ['name'=>'Finance I','slug'=>'finance-i','status'=>'active','color'=>'#ed417b','created_by' => 1],
            ['name'=>'Finance II','slug'=>'finance-ii','status'=>'active','color'=>'#0391d1','created_by' => 1],
            ['name'=>'Account I','slug'=>'account-i','status'=>'active','color'=>'#8bc34a','created_by' => 1],
            ['name'=>'Account II','slug'=>'account-ii','status'=>'active','color'=>'#8bc34a','created_by' => 1],
            ['name'=>'Artificial Intelligence','slug'=>'artificial-intelligence','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Cloud Computing','slug'=>'cloud-computing','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Data Communications','slug'=>'data-communications','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Data Mining and Data Warehousing','slug'=>'data-mining-and-data-warehousing','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Database Management System','slug'=>'database-management-system','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Digital Image Processing','slug'=>'digital-image-processing','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Distributed Database System','slug'=>'distributed-database-system','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Operating System','slug'=>'operating-system','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Real Time System','slug'=>'real-time-system','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Semantic Web Technologies','slug'=>'semantic-web-technologies','status'=>'active','color'=>'#ab47bc','created_by' => 1],
            ['name'=>'Visual Programming (VP)','slug'=>'visual-programming-vp','status'=>'active','color'=>'#ab47bc','created_by' => 1],
        ];
        foreach($subjects as $subject){

            Subject::create($subject);
        }
    }
}