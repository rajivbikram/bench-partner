<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name'      => 'Rajiv Bikram',
            'email'     =>  'admin@admin.com',
            'password'  =>  bcrypt('password'),
            'username' =>   'admin',
            'role_id'   =>  1,
            'status'    =>  'active',
        ]);

        User::create([
            'name'      => 'Editor',
            'email'     =>  'editor@editor.com',
            'password'  =>  bcrypt('password'),
            'username' =>   'editor',
            'role_id'   =>  2,
            'status'    =>  'active',
        ]);

        User::create([
            'name'      => 'User',
            'email'     =>  'user@user.com',
            'password'  =>  bcrypt('password'),
            'username' =>   'user',
            'role_id'   =>  3,
            'status'    =>  'active',
        ]);

    }
}
