

<!-- Scripts -->
<script src="{{ asset('js/all.js') }}" defer></script>

@livewireScripts

<!-- Turbolinks JS -->
<script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>

@stack('scripts')