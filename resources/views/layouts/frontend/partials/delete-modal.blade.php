
<div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
            <div class="text-primary">
                <i class="h1 ai-info text-muted"></i>
                <h5 class="alert-heading">Are You Sure ?</h5>
                <p>This will be deleted</p>
            </div>
            <div class="pb-2">
                <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal">Cancel</button>
                <button class="btn btn-danger btn-shadow btn-sm" wire:click.prevent="delete" type="button">
                  <span wire:loading.remove wire:target="delete">
                    <i class="ai-trash-2 me-1"></i> Delete</span>
                    <span wire:loading wire:target="delete"> 
                    <span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span> 
                      Deleting...
                    </span>
                </button>
            </div>
        </div>
      </div>
    </div>
  </div>