<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="/frontend/image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="/frontend/image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#5bbad5" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>
  

    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="/frontend/css/theme.min.css">

    @livewireStyles

    @stack('styles')

</head>
 <body style="background-color:#f7f7fc;">
    <div id="app">

            <main>
              <!-- Navbar (Floating light)-->
              <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
              @include('layouts.frontend.navbar')
              <!-- Page content-->
              <!-- Slanted background-->
              <div class="position-relative bg-gradient" style="height: 480px;">
                <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
                    <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
                  </svg>
                </div>
              </div>
              <!-- Page content-->
                @yield('content')
            </main>
        
  

        <footer class="footer py-4">
          <div class="container d-md-flex align-items-center justify-content-between py-2 text-center text-md-end">
            <ul class="list-inline fs-sm mb-3 mb-md-0 order-md-2">
              <li class="list-inline-item my-1"><a class="nav-link-style" href="#">Support</a></li>
              <li class="list-inline-item my-1"><a class="nav-link-style" href="#">Contacts</a></li>
              <li class="list-inline-item my-1"><a class="nav-link-style" href="#">Terms &amp; Conditions</a></li>
            </ul>
            <p class="fs-sm mb-0 me-3 order-md-1"><span class="text-muted me-1">© All rights reserved. Made by</span><a class="nav-link-style fw-normal" href="https://createx.studio/" target="_blank" rel="noopener">Createx Studio</a></p>
          </div>
        </footer>

        <!-- Back to top button-->
        <a class="btn-scroll-top" href="#top" data-scroll data-fixed-element><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ai-arrow-up">   </i></a>

    </div>

    @stack('scripts')

    <!-- Scripts -->
    <script src="{{ asset('js/all.js') }}" defer></script>

     @livewireScripts

    <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>

    

</body>
</html>
