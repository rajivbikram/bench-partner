<div class="col-lg-3 mb-4 mb-lg-0">
    <div class="bg-light rounded-3 shadow-lg">
      <div class="px-4 py-4 mb-1 text-center">
        <img class="d-block rounded-circle mx-auto my-2" src="/backend/img/avatars/profiles/avatar-6.jpg" at="{{ auth()->user()->name }}" width="80">
        <h6 class="mb-0 pt-1">{{ auth()->user()->name }}</h6><span class="text-muted fs-sm">{{ $username }}</span>
      </div>
      <div class="d-lg-none px-4 pb-4 text-center">
        <a class="btn btn-primary px-5 mb-2 collapsed" href="#account-menu" data-bs-toggle="collapse" aria-expanded="false"><i class="ai-menu me-2"></i>Menu</a>
      </div>
      <div class="d-lg-block pb-2 collapse" id="account-menu" style="">

        <h3 class="d-block bg-secondary fs-sm fw-semibold text-muted mb-0 px-4 py-2">Dashboard</h3>
        
        @role('admin','creator')
          <a class="d-flex align-items-center nav-link-style px-4 py-3 {{\Request::segment(2) == 'subjects'?'active':''}}" href="{{ route('backend.subject.index', $username) }}">
            <i class="ai-book fs-lg opacity-60 me-2"></i>Subjects
          </a>

          <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top {{\Request::segment(2) == 'chapters'?'active':''}}" href="{{ route('backend.chapter', $username) }}">
            <i class="ai-file fs-lg opacity-60 me-2"></i>Chapters
          </a>
        @endrole

        <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top {{\Request::segment(2) == 'posts'?'active':''}}" href="{{ route('backend.post.index', $username) }}">
          <i class="ai-file-text fs-lg opacity-60 me-2"></i>Posts
        </a>

        @role('admin','creator')
          <h3 class="d-block bg-secondary fs-sm fw-semibold text-muted mb-0 px-4 py-2">Users</h3>

          <a class="d-flex align-items-center nav-link-style px-4 py-3" href="{{ route('backend.user.index', $username) }}">
            <i class="ai-users fs-lg opacity-60 me-2"></i>Users
          </a>
          <a class="d-flex align-items-center nav-link-style border-top px-4 py-3 {{\Request::segment(2) == 'roles'?'active':''}}" href="{{ route('backend.role', $username) }}">
            <i class="ai-disc fs-lg opacity-60 me-2"></i>Roles
          </a>
        @endrole

        <h3 class="d-block bg-secondary fs-sm fw-semibold text-muted mb-0 px-4 py-2">Settings</h3>

        <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top {{\Request::segment(2) == 'profile'?'active':''}}" href="{{ route('backend.profile', $username) }}">
          <i class="ai-user fs-lg opacity-60 me-2"></i>Profile Info
        </a>

        @role('admin')
          <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top {{\Request::segment(2) == 'settings'?'active':''}}" href="{{ route('backend.setting', $username) }}">
            <i class="ai-settings fs-lg opacity-60 me-2"></i>Settings
          </a>
          <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top" href="#"><i class="ai-mail fs-lg opacity-60 me-2"></i>News Letter</a>
        @endrole

        @role('creator','editor','user')
          <div class="d-flex align-items-center border-top">
            <a class="d-block w-100 nav-link-style px-4 py-3" href="account-notifications.html">Notifications</a>
            <div class="ms-auto px-3">
              <div class="form-check form-switch">
                <input class="form-check-input" type="checkbox" id="notifications-switch" data-master-checkbox-for="#notification-settings" checked>
                <label class="form-check-label" for="notifications-switch"></label>
              </div>
            </div>
          </div>
        @endrole

        <a class="d-flex align-items-center nav-link-style px-4 py-3 border-top" href="signin-illustration.html"><i class="ai-log-out fs-lg opacity-60 me-2"></i>Sign out</a>
      </div>
    </div>
  </div>