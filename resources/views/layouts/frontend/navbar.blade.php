@if (request()->routeIs('login') || request()->routeIs('register'))
    <header class="header navbar navbar-expand-lg navbar-light navbar-floating navbar-sticky">
@else
    <header class="header navbar navbar-expand-lg navbar-light bg-light navbar-shadow navbar-sticky">
@endif

        <div class="navbar-search bg-light">
          <div class="container d-flex flex-nowrap align-items-center"><i class="ai-search fs-xl"></i>
            <input class="form-control form-control-xl navbar-search-field" type="text" placeholder="Search site">
            <div class="d-flex align-items-center"><span class="text-muted fs-xs d-none d-sm-inline">Close</span>
              <button class="btn-close p-2" type="button" data-bs-toggle="search"></button>
            </div>
          </div>
        </div>
        <div class="container px-0 px-xl-3">
          <button class="navbar-toggler ms-n2 me-4" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span class="navbar-toggler-icon"></span></button><a class="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4" href="{{ route('home') }}"><img class="d-none d-lg-block" src="/frontend/img/logo/logo-dark.png" alt="Around" width="153"><img class="d-lg-none" src="/frontend/img/logo/logo-icon.png" alt="Around" width="58"></a>
          <div class="d-flex align-items-center order-lg-3 ms-lg-auto">
            <div class="navbar-tool">
              <a class="navbar-tool-icon-box me-lg-2" href="#" data-bs-toggle="search"><i class="ai-search"></i></a>
            </div>            
           
            @guest
                 <div class="d-lg-block d-none text-nowrap">
                  <a class="nav-link-style fs-sm me-3 text-nowrap" href="{{ route('login') }}"><i class="ai-user fs-xl me-2 align-middle"></i>Log in</a>
                  <a class="btn btn-primary btn-sm d-none d-lg-inline-block" href="{{ route('register') }}">Sign up</a>
                </div>
            @endguest

            @auth
                <div class="d-flex align-items-center order-lg-3 ms-lg-auto">
                  <div class="navbar-tool dropdown">
                    <a class="navbar-tool-icon-box" href="#">
                      <img class="navbar-tool-icon-box-img" src="/frontend/img/dashboard/avatar/main-sm.jpg" alt="Avatar">
                    </a>
                    <a class="navbar-tool-label dropdown-toggle" href="#"><small>Hello,</small>
                      {{ auth()->user()->name }}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" style="width: 15rem;">
                      <li>
                        <a class="dropdown-item d-flex align-items-center" href="{{ route('backend.dashboard',$username) }}">
                          <i class="ai-shopping-bag fs-base opacity-60 me-2"></i>Dashboard
                        </a>
                      </li>
                      <li class="dropdown-divider"></li>
                      <li>
                        <a class="dropdown-item d-flex align-items-center" href="{{ route('backend.setting',$username) }}">
                          <i class="ai-settings fs-base opacity-60 me-2"></i>Settings<span class="nav-indicator">
                        </a>
                      </li>
                      <li class="dropdown-divider"></li>
                      <li>
                        <a class="dropdown-item d-flex align-items-center" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                          <i class="ai-log-out fs-base opacity-60 me-2"></i>Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                      </li>
                    </ul>
                  </div>
                </div>
            @endauth
          </div>
          <div class="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
            <div class="offcanvas-cap navbar-shadow">
              <h5 class="mt-1 mb-0">Menu</h5>
              <button class="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <!-- Menu-->
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="#" data-bs-toggle="dropdown">Programs</a>
                </li>
                <li class="nav-item dropdown dropdown-mega"><a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Information Technology</a>
                  <div class="dropdown-menu"><a class="dropdown-column dropdown-column-img bg-secondary" href="{{ route('home') }}" style="background-image: url('/frontend/img/demo/menu-banner.jpg');"></a>
                    <div class="dropdown-column"><a class="dropdown-item" href="{{ route('home') }}">Web Template Presentation</a><a class="dropdown-item" href="demo-business-consulting.html">Business Consulting</a><a class="dropdown-item" href="demo-shop-homepage.html">Shop Homepage</a><a class="dropdown-item" href="demo-booking-directory.html">Booking / Directory</a><a class="dropdown-item" href="demo-creative-agency.html">Creative Agency</a><a class="dropdown-item" href="demo-web-studio.html">Web Studio</a><a class="dropdown-item" href="demo-product-software.html">Product Landing - Software</a></div>
                    <div class="dropdown-column"><a class="dropdown-item" href="demo-product-gadget.html">Product Landing - Gadget</a><a class="dropdown-item" href="demo-mobile-app.html">Mobile App Showcase</a><a class="dropdown-item" href="demo-coworking-space.html">Coworking Space</a><a class="dropdown-item" href="demo-event-landing.html">Event Landing</a><a class="dropdown-item" href="demo-marketing-seo.html">Digital Marketing &amp; SEO</a><a class="dropdown-item" href="demo-food-blog.html">Food Blog</a><a class="dropdown-item" href="demo-personal-portfolio.html">Personal Portfolio</a></div>
                  </div>
                </li>
                <li class="nav-item dropdown dropdown-mega"><a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Management</a>
                  <div class="dropdown-menu"><a class="dropdown-column dropdown-column-img bg-secondary" href="{{ route('home') }}" style="background-image: url('/frontend/img/demo/menu-banner.jpg');"></a>
                    <div class="dropdown-column"><a class="dropdown-item" href="{{ route('home') }}">Web Template Presentation</a><a class="dropdown-item" href="demo-business-consulting.html">Business Consulting</a><a class="dropdown-item" href="demo-shop-homepage.html">Shop Homepage</a><a class="dropdown-item" href="demo-booking-directory.html">Booking / Directory</a><a class="dropdown-item" href="demo-creative-agency.html">Creative Agency</a><a class="dropdown-item" href="demo-web-studio.html">Web Studio</a><a class="dropdown-item" href="demo-product-software.html">Product Landing - Software</a></div>
                    <div class="dropdown-column"><a class="dropdown-item" href="demo-product-gadget.html">Product Landing - Gadget</a><a class="dropdown-item" href="demo-mobile-app.html">Mobile App Showcase</a><a class="dropdown-item" href="demo-coworking-space.html">Coworking Space</a><a class="dropdown-item" href="demo-event-landing.html">Event Landing</a><a class="dropdown-item" href="demo-marketing-seo.html">Digital Marketing &amp; SEO</a><a class="dropdown-item" href="demo-food-blog.html">Food Blog</a><a class="dropdown-item" href="demo-personal-portfolio.html">Personal Portfolio</a></div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="offcanvas-cap justify-content-center border-top">
              <a class="nav-link-style fs-sm text-nowrap" href="{{ route('login') }}"><i class="ai-user fs-xl me-2 align-middle"></i>Log in</a>
              <a class="btn-social bs-lg bs-outline bs-facebook me-3" href="#"><i class="ai-facebook"></i></a>
              <a class="btn-social bs-lg bs-outline bs-twitter me-3" href="#"><i class="ai-twitter"></i></a>
              <a class="btn-social bs-lg bs-outline bs-instagram me-3" href="#"><i class="ai-instagram"></i></a>
              <a class="btn-social bs-lg bs-outline bs-pinterest" href="#"><i class="ai-pinterest"></i></a>
            </div>
          </div>
        </div>
      </header>