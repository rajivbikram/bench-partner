<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

  @include('layouts.frontend.head')

  {{-- <body> --}}
    <body {{ $bodyClass }}>

    <div id="app">

            <main>

            @include('layouts.frontend.navbar')

            {{ $slot }}

            </main>

            @include('layouts.frontend.footer')

    </div>

    @include('layouts.frontend.js')

  
    </body>

</html>
