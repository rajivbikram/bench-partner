@section('title', 'Create an Account')
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

  @include('layouts.frontend.head')

  <body>

    <div id="app" class="page-wrapper d-flex flex-column">
    
    <main class="page-wrapper d-flex">
      <!-- Navbar (Floating dark)-->
      <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
      @include('layouts.frontend.navbar')
      <div class="d-none d-md-block position-absolute w-50 h-100 bg-size-cover" style="top: 0; right: 0; background-image: url(/frontend/img/login.svg);"></div>
      <!-- Page content-->
      <section class="container d-flex align-items-center pt-7 pb-3 pb-md-4" style="flex: 1 0 auto;">
        <div class="w-100 pt-3">
          <div class="row">
            <div class="col-lg-4 col-md-6 offset-lg-1">
              <!-- Login view-->
              <div class="view show" id="signup-view">
                <h1 class="h2">Create an Account</h1>
                <p class="fs-ms text-muted mb-4">Sign up for free and join the one of the Best Community of Skilled Peoples.</p>
                <form class="needs-validation" novalidate="" method="POST" action="{{ route('register') }}">
                  @csrf
                  <div class="mb-3">
                    <input id="name" type="text" placeholder="Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-group mb-3">
                    <div class="password-toggle w-100">
                      <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required>

                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                      <label class="password-toggle-btn" aria-label="Show/hide password">
                        <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                      </label>
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="password-toggle w-100">
                      <input id="password-confirm" type="password" placeholder="Confirm password" class="form-control" name="password_confirmation" required>
                      <label class="password-toggle-btn" aria-label="Show/hide password">
                        <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                      </label>
                    </div>
                  </div>
                  <button class="btn btn-primary d-block w-100" type="submit">Sign up</button>
                  <p class="fs-sm pt-3 mb-0">Already have an account? <a href="{{ route('login') }}" class="fw-medium">Login</a></p>
                </form>
              </div>
              <div class="border-top text-center mt-4 pt-4">
                <p class="fs-sm fw-medium text-heading">Or sign up with</p><a class="btn-social bs-facebook bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-instagram bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-instagram"></i></a><a class="btn-social bs-google bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-google"></i></a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    @include('layouts.frontend.footer')

    </div>

    @include('layouts.frontend.js')

  </body>

<!-- Mirrored from around.createx.studio/signin-illustration.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Mar 2021 15:09:56 GMT -->
</html>