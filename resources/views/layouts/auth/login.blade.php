@section('title', 'Login')
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

  @include('layouts.frontend.head')

  <body>

    <div id="app" class="page-wrapper d-flex flex-column">
    
    <main class="page-wrapper d-flex">
      <!-- Navbar (Floating dark)-->
      <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
      @include('layouts.frontend.navbar')
      <div class="d-none d-md-block position-absolute w-50 h-100 bg-size-cover" style="top: 0; right: 0; background-image: url(/frontend/img/login.svg);"></div>
      <!-- Page content-->
      <section class="container d-flex align-items-center pt-7 pb-3 pb-md-4" style="flex: 1 0 auto;">
        <div class="w-100 pt-3">
          <div class="row">
            <div class="col-lg-4 col-md-6 offset-lg-1">
              <!-- Login view-->
              <div class="view show" id="signin-view">
                <h1 class="h2">Login</h1>
                <p class="fs-ms text-muted mb-4">Login to your account using email and password provided during registration.</p>
                <form class="needs-validation" method="POST" action="{{ route('login') }}" novalidate="">
                  @csrf
                  <div class="input-group mb-3"><i class="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                    <input id="email" type="email" class="form-control rounded @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required="">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="input-group mb-3"><i class="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                    <div class="password-toggle w-100">
                      <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                      <label class="password-toggle-btn" aria-label="Show/hide password">
                        <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                      </label>
                    </div>
                  </div>
                  <div class="d-flex justify-content-between align-items-center mb-3 pb-1">
                    <div class="form-check">
                      <input class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} type="checkbox">
                      <label class="form-check-label" for="remember">Keep me Login</label>
                    </div><a class="nav-link-style fs-ms" href="password-recovery.html">Forgot password?</a>
                  </div>
                  <button class="btn btn-primary d-block w-100" type="submit">Login</button>
                  <p class="fs-sm pt-3 mb-0">Don't have an account? <a href="{{ route('register') }}" class="fw-medium" data-view="#signup-view">Sign up</a></p>
                </form>
              </div>
              <div class="border-top text-center mt-4 pt-4">
                <p class="fs-sm fw-medium text-heading">Or login with</p><a class="btn-social bs-facebook bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-instagram bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-instagram"></i></a><a class="btn-social bs-google bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-google"></i></a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    @include('layouts.frontend.footer')

    </div>

    @include('layouts.frontend.js')

  </body>

<!-- Mirrored from around.createx.studio/signin-illustration.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Mar 2021 15:09:56 GMT -->
</html>