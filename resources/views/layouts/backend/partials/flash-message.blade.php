@if ($message = Session::get('success'))

	<div class="alert alert-success alert-dismissible d-flex fade show" role="alert">
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	    <i class="ai-check-circle fs-xl me-2"></i> <strong class="me-2">Success !</strong> {{ $message }}
	</div>

@endif


@if ($message = Session::get('error'))

	<div class="alert alert-danger alert-dismissible d-flex fade show" role="alert">
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	    <i class="ai-x-circle fs-xl me-2"></i> <strong class="me-2">Error !</strong> {{ $message }}
	</div>

@endif


@if ($message = Session::get('warning'))

	<div class="alert alert-warning alert-dismissible d-flex fade show" role="alert">
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		<i class="ai-alert-triangle fs-xl me-2"></i> <strong class="me-2">Warning !</strong> {{ $message }}
	</div>

@endif


@if ($message = Session::get('info'))

	<div class="alert alert-info alert-dismissible d-flex fade show" role="alert">
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		<i class="ai-bell fs-xl me-2"></i> <strong class="me-2">Information !</strong> {{ $message }}
	</div>

@endif


@if ($errors->any())
	@foreach ($errors->all() as $message)
		<div class="alert alert-danger alert-dismissible d-flex fade show" role="alert">
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			<i class="ai-x-circle fs-xl me-2"></i> <strong class="me-2">Error !</strong> {{ $message }}
		</div>
	@endforeach
@endif