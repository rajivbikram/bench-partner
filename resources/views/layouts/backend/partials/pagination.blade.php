@if ($paginator->hasPages())
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
             <ul class="pagination pagination-tabs card-pagination">
              <li class="page-item">
                <span class="page-link active ps-0 pe-4 border-end">
                  {!! __('pagination.previous') !!}
                </span>
              </li>
            </ul>
        @else
            <ul class="list-pagination-prev pagination pagination-tabs card-pagination">
              <li class="page-item">
                <a class="page-link ps-0 pe-4 border-end" href="{{ $paginator->previousPageUrl() }}">
                  {!! __('pagination.previous') !!}
                </a>
              </li>
            </ul>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <ul class="pagination pagination-tabs card-pagination">
              <li class="page-item">
                <a class="page-link ps-4 pe-0 border-start" href="{{ $paginator->nextPageUrl() }}">
                  {!! __('pagination.next') !!}
                </a>
              </li>
            </ul>
        @else
            <ul class="list-pagination-next pagination pagination-tabs card-pagination">
              <li class="page-item">
                <a class="page-link ps-4 pe-0 border-start" href="#">
                  {!! __('pagination.next') !!}
                </a>
              </li>
            </ul>
        @endif
@endif