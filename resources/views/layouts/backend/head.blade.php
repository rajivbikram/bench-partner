

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />
    
    <!-- Title -->
    <title>@yield('title')</title>
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="/backend/favicon/favicon.ico" type="image/x-icon"/>
    

    <!-- Libs CSS -->
    <link rel="stylesheet" href="/backend/css/libs.bundle.css" />
    
    <!-- Theme CSS -->
    <link rel="stylesheet" href="/backend/css/theme.bundle.css" id="stylesheetLight" />
    {{-- <link rel="stylesheet" href="/backend/css/theme-dark.bundle.css" id="stylesheetDark" /> --}}

    <script src="{{ asset('js/app.js') }}" defer></script>

    @livewireStyles

    @stack('backendStyles')
    
        
    
   
  </head>