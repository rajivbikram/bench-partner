<nav class="navbar navbar-vertical fixed-start navbar-expand-md navbar-dark">
      <div class="container-fluid">
    
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    
        <!-- Brand -->
        <a class="navbar-brand" href="index-2.html">
          <img src="/backend/img/logo.svg" class="navbar-brand-img mx-auto" alt="...">
        </a>
    
        <!-- User (xs) -->
        <div class="navbar-user d-md-none">
    
          <!-- Dropdown -->
          <div class="dropdown">
    
            <!-- Toggle -->
            <a href="#" id="sidebarIcon" class="dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="avatar avatar-sm avatar-online">
                <img src="/backend/img/avatars/profiles/avatar-1.jpg" class="avatar-img rounded-circle" alt="...">
              </div>
            </a>
    
            <!-- Menu -->
            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="sidebarIcon">
              <a href="profile-posts.html" class="dropdown-item">Profile</a>
              <a href="account-general.html" class="dropdown-item">Settings</a>
              <hr class="dropdown-divider">
              <a href="sign-in.html" class="dropdown-item">Logout</a>
            </div>
    
          </div>
    
        </div>
    
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidebarCollapse">
    
          <!-- Form -->
          <form class="mt-4 mb-3 d-md-none">
            <div class="input-group input-group-rounded input-group-merge input-group-reverse">
              <input class="form-control" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-text">
                <span class="fe fe-search"></span>
              </div>
            </div>
          </form>
    
          <!-- Navigation -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{\Request::segment(2) == 'dashboard'?'active':''}}" href="{{ route('backend.dashboard') }}">
                <i class="fe fe-home"></i> Dashboards
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{\Request::segment(2) == 'categories'?'active':''}}" href="{{ route('backend.category') }}">
                <i class="fe fe-folder"></i> Categories
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{\Request::segment(2) == 'chapters'?'active':''}}" href="{{ route('backend.category') }}">
                <i class="fe fe-list"></i> Chapters
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#sidebarBasics" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarBasics">
                <i class="fe fe-file"></i> Posts
              </a>
              <div class="collapse " id="sidebarBasics">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="{{ route('backend.post.index') }}" class="nav-link ">
                      Post
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('backend.post.create') }}" class="nav-link ">
                      Create
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
    
          <!-- Divider -->
          <hr class="navbar-divider my-3">
    
          <!-- Heading -->
          <h6 class="navbar-heading">
            Settings
          </h6>
    
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-4">
            <li class="nav-item">
              <a class="nav-link {{\Request::segment(2) == 'roles'?'active':''}}" href="{{ route('backend.role') }}">
                <i class="fe fe-folder"></i> Roles
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{\Request::segment(2) == 'settings'?'active':''}}" href="{{ route('backend.setting') }}">
                <i class="fe fe-setting"></i> Settings
              </a>
            </li>
          </ul>
    
          <!-- Push content down -->
          <div class="mt-auto"></div>
    
            <!-- User (md) -->
            <div class="navbar-user d-none d-md-flex" id="sidebarUser">
    
              <!-- Icon -->
              <a href="#sidebarModalActivity" class="navbar-user-link" data-bs-toggle="modal">
                <span class="icon">
                  <i class="fe fe-bell"></i>
                </span>
              </a>
    
              <!-- Dropup -->
              <div class="dropup">
    
                <!-- Toggle -->
                <a href="#" id="sidebarIconCopy" class="dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <div class="avatar avatar-sm avatar-online">
                    <img src="/backend/img/avatars/profiles/avatar-1.jpg" class="avatar-img rounded-circle" alt="...">
                  </div>
                </a>
    
                <!-- Menu -->
                <div class="dropdown-menu" aria-labelledby="sidebarIconCopy">
                  <a href="profile-posts.html" class="dropdown-item">Profile</a>
                  <a href="account-general.html" class="dropdown-item">Settings</a>
                  <hr class="dropdown-divider">
                  <a href="sign-in.html" class="dropdown-item">Logout</a>
                </div>
    
              </div>
    
              <!-- Icon -->
              <a href="#sidebarModalSearch" class="navbar-user-link" data-bs-toggle="modal">
                <span class="icon">
                  <i class="fe fe-search"></i>
                </span>
              </a>
    
            </div>
    
        </div> <!-- / .navbar-collapse -->
    
      </div>
    </nav>
