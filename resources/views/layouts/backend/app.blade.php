<!DOCTYPE html>
<html lang="en">
  


@include('layouts.backend.head')  

<!-- Body-->
  <body>
    <div id="app">
      <!-- Page loading spinner-->
      @include('layouts.backend.sidebar')  

      <div class="main-content">

        @include('layouts.backend.navbar')
        <!-- Page content-->
        <!-- Slanted background-->
        
        <!-- Page content-->
        @yield('content')

      </div>
      <!-- Footer-->
    </div>
    
    @include('layouts.backend.footer')

  </body>


</html>