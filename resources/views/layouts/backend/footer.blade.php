	    
    <!-- Vendor JS -->
    <script src="/backend/js/vendor.bundle.js"></script>
    
    <!-- Theme JS -->
    <script src="/backend/js/theme.bundle.js"></script>

	@livewireScripts
    
    @stack('backendScripts')

    <!-- Turbolinks JS -->
    <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>