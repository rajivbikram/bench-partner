<x-dashboard-layout>
    <div>
        <div class="position-relative bg-gradient" style="height: 480px;">
            <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
                <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
              </svg>
            </div>
          </div>
        <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -400px;">
              <div class="row">
                <!-- Sidebar-->
                 @include('layouts.frontend.dashboard.sidebar')
                <!-- Content-->
                <div class="col-lg-9">
                  <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                    <div class="pt-2 pt-0 p-md-3">
                      <!-- Title-->
                      <h1 class="h3 mb-2 text-nowrap">Dashboard</h1>
                      <div class="pb-4">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                              <a href="{{ route('home') }}">
                                <i class="ai-home"></i>
                                Home
                              </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                          </ol>
                        </nav>
                      </div>
                      <!-- Stats-->
                      <div class="row mx-n2 py-2">
                        <div class="col-md-4 col-sm-6 px-2 mb-3">
                          <div class="bg-secondary h-100 rounded-3 p-4 text-center">
                            <h3 class="fs-sm fw-medium text-body">Earnings (before taxes)</h3>
                            <p class="h2 mb-2">$842.00</p>
                            <p class="fs-ms text-muted mb-0">Sales 8/1/2019 - 8/15/2019</p>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6 px-2 mb-3">
                          <div class="bg-secondary h-100 rounded-3 p-4 text-center">
                            <h3 class="fs-sm fw-medium text-body">Your balance</h3>
                            <p class="h2 mb-2">$735.00</p>
                            <p class="fs-ms text-muted mb-0">To be paid on 8/15/2019</p>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-12 px-2 mb-3">
                          <div class="bg-secondary h-100 rounded-3 p-4 text-center">
                            <h3 class="fs-sm fw-medium text-body">Lifetime earnings</h3>
                            <p class="h2 mb-2">$9,156.74</p>
                            <p class="fs-ms text-muted mb-0">Based on list price</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  <div>
                  </div>
                </div>
              </div>
      
      </div> 
</x-dashboard-layout>