<!DOCTYPE html>
<html lang="en">
  

@section('title', 'Create new Post')

@include('layouts.frontend.head')  


<!-- Body-->
  <body>
    <div id="app">

      <div class="main-content">

        @livewire('backend.post.create')

      </div>

      <!-- Back to top button-->
      <a class="btn-scroll-top" href="#top" data-scroll data-fixed-element><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ai-arrow-up">   </i></a>

    </div>

  </body>

  @livewireScripts
   
<!-- Vendor JS -->
<script src="/backend/js/vendor.bundle.js"></script>

<!-- Theme JS -->
<script src="/backend/js/theme.bundle.js"></script>

  @stack('scripts')


</html>