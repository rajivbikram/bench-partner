@section('title', $subject->name)
<div>

      <section class="position-relative bg-secondary pt-3 pt-lg-6 pb-4 overflow-hidden">
        <div class="shape shape-bottom shape-curve-side bg-body" style="z-index: 0">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 250">
              <path fill="currentColor" d="M3000,0v250H0v-51c572.7,34.3,1125.3,34.3,1657.8,0C2190.3,164.8,2637.7,98.4,3000,0z"></path>
            </svg>
          </div>
        <!-- Content-->
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 py-3 py-lg-0">
                  <h1 class="mb-4">{{ $subject->name }}</h1>
                  @if ($subject->description)
                      <div class="py-4">
                          <p>{{ $subject->description }}</p>
                      </div>
                  @endif
                </div>
                <div class="col-lg-4">
                </div>
                <!-- Media tabs: Horizontal -->
                <div class="col-lg-12">
                    <ul class="nav subject mb-2 nav-tabs media-tabs position-relative" role="tablist">
                        <li class="nav-item me-3 mb-3">
                            <a class="nav-link active" href="#">
                                <div class="d-flex align-items-center">
                                    <i class="ai-book text-primary lead mb-0 me-1 ms-2"></i>
                                    <div class="ms-1">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="pe-1">Chapters</div>
                                            <i class="ai-chevron-right me-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item me-3 mb-3">
                            <a class="nav-link" href="#">
                                <div class="d-flex align-items-center">
                                    <i class="ai-alert-circle text-warning lead mb-0 me-1 ms-2"></i>
                                    <div class="ms-1">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="pe-1">Questions</div>
                                            <i class="ai-chevron-right me-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item me-3 mb-3">
                            <a class="nav-link" href="#">
                                <div class="d-flex align-items-center">
                                    <i class="ai-file-text text-success lead mb-0 me-1 ms-2"></i>
                                    <div class="ms-1">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="pe-1">Notes</div>
                                            <i class="ai-chevron-right me-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item me-3 mb-3">
                            <a class="nav-link" href="#">
                                <div class="d-flex align-items-center">
                                    <i class="ai-download text-danger lead mb-0 me-1 ms-2"></i>
                                    <div class="ms-1">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="pe-1">Downloads</div>
                                            <i class="ai-chevron-right me-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
              </div>
        </div>
      </section>

      <section class="py-4">
          <div class="container">
              <div class="row">
                <div class="col-lg-3 pt-lg-5">
                    <div class="offcanvas offcanvas-collapse" id="blog-sidebar">
                      <div class="offcanvas-cap navbar-shadow px-4 mb-3">
                        <h5 class="mt-1 mb-0">Sidebar</h5>
                        <button class="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                      </div>
                      <div class="offcanvas-body px-4 pt-3 pt-lg-0 ps-lg-0 pe-lg-2 pe-xl-4" data-simplebar>
                        <!-- Categories-->
                        <div class="widget widget-categories mb-5">
                          <h3 class="widget-title">Chapters</h3>
                          <ul>
                            @if (!empty($subject->chapters))
                                @foreach ($subject->chapters as $chapter)
                                    <li>
                                        <a class="widget-link" href="#{{ $chapter->slug }}-{{ $chapter->id }}">{{ $chapter->name }}
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="col-lg-9 content order-1 order-sm-1 order-md-2">
                      <!-- Basic accordion -->
                    <div class="accordion" id="accordionExample">
    
                        <!-- Item -->
                        @if (!empty($subject->chapters))
                            @foreach ($subject->chapters as $chapter)
                                <div class="accordion-item mb-4 bg-secondary">
                                    <h2 class="accordion-header" id="{{ $chapter->slug }}-{{ $chapter->id }}">
                                        <div class="py-3 px-4 fs-5 border-bottom">{{ $loop->index + 1}}. {{ $chapter->name }}</div>
                                    </h2>
                                    <div class="accordion-collapse collapse show">
                                        <div class="accordion-body pt-3">
                                            <div class="widget widget-categories">
                                                <ul class="pt-2">
                                                    @if (count($chapter->posts)>0)
                                                        @foreach ($chapter->posts as $post)
                                                            <li class="topic pb-2 opacity-90"> 
                                                                <a class="d-flex chapter-post" title="{{ $post->post_title }}" href="{{ route('frontend.post.details', $post->post_slug ) }}">
                                                                    <span class="d-table">
                                                                        <i class="ai-arrow-right-circle mb-1 fs-4 me-2 text-muted"></i>
                                                                        {{-- <i class="ai-check-circle fs-5 me-2 text-muted align-middle"></i>  --}}
                                                                    </span>
                                                                    {{ $post->post_title }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <li class="topic pb-2 opacity-90"> 
                                                            <a class="d-flex chapter-post align-middle" title="Coming Soon" href="#">
                                                                <span class="d-table">
                                                                    <i class="ai-info mb-1 fs-4 me-2 text-muted"></i>
                                                                    {{-- <i class="ai-check-circle fs-5 me-2 text-muted align-middle"></i>  --}}
                                                                </span>
                                                                Coming Soon
                                                            </a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                  </div>
              </div>
          </div>
      </section>

</div>