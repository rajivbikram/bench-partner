@section('title', 'Categories')
<div>
  
  	<!-- Hero-->
      <section class="d-md-block d-none py-7 position-relative bg-no-repeat bg-position-center" style="background-image: url(/frontend/img/demo/food-blog/hero/bg.jpg);"><img class="d-lg-block d-none position-absolute mb-n5" src="/frontend/img/demo/food-blog/hero/bg-shape.png" alt="Shape" style="bottom: 0; left: 0;" data-jarallax-element="25" data-disable-parallax-down="md">
        <div class="container py-lg-7 py-md-6 py-5">
          <div class="row">
            <div class="col-xl-6 col-md-8 col-sm-10">
              <h1 class="display-4 mb-4 pb-3">Enjoy the new cookbook by Amelie Mayer</h1><a class="btn btn-primary" href="#book" data-scroll>Get the book</a>
            </div>
          </div>
        </div>
      </section>
      <!-- Hero mobile-->
      <section class="container d-md-none d-block px-0"><img class="d-block mx-auto mb-grid-gutter" src="/frontend/img/demo/food-blog/hero/bg-m.jpg" alt="Amelie Mayer">
        <div class="mx-auto px-3 text-center" style="max-width: 500px;">
          <h1 class="display-4 mb-4">Enjoy the new cookbook by Amelie Mayer</h1><a class="btn btn-primary" href="#book" data-scroll>Get the book</a>
        </div>
      </section>
      <!-- Featured recipes-->
      <section class="container pt-5 mt-4 mt-sm-0 pt-sm-6 pt-md-7 pb-lg-7">
        <h2 class="h1 text-center">Featured recipes</h2>
        <!-- Carousel-->
        <div class="tns-carousel-wrapper">
          <!-- Carousel progress-->
          <div class="tns-carousel-progress ms-auto mb-3">
            <div class="text-sm text-muted text-center mb-2"><span class="tns-current-slide me-1"></span>of<span class="tns-total-slides ms-1"></span></div>
            <div class="progress">
              <div class="progress-bar bg-primary" role="progressbar"></div>
            </div>
          </div>
          <div class="tns-carousel-inner  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" data-carousel-options="{&quot;nav&quot;: false, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1, &quot;gutter&quot;: 16, &quot;controls&quot;: false},&quot;500&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 16},&quot;768&quot;:{&quot;items&quot;:3, &quot;gutter&quot;: 16},&quot;991&quot;:{&quot;controls&quot;: true}}}" id="tns1" style="transform: translate3d(-43.75%, 0px, 0px);">
            <!-- Carousel item-->
            <article class="pb-2"><a class="card h-100 border-0 shadow card-hover mx-1" href="#"><span class="badge badge-lg badge-floating badge-floating-end bg-success">Vegetarian</span>
                <div class="card-img-top card-img-gradient"><img src="/frontend/img/demo/food-blog/featured-recipes/01.jpg" alt="Vegan Raw Seasoning Appetizers"><span class="card-floating-text text-light fw-medium">Discover this recipe<i class="ai-chevron-right fs-lg ms-1 mt-n1 align-middle"></i></span></div>
                <div class="card-body">
                  <div class="mb-3 text-nowrap"><span class="meta-link fs-xs"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Oct 8</span><span class="meta-divider"></span><span class="meta-link fs-xs"><i class="ai-message-square me-1"></i>&nbsp;3</span></div>
                  <h3 class="h5 pt-1 mb-3">Vegan Raw Seasoning Appetizers</h3>
                  <p class="fs-sm text-muted mb-2">10 min — 2-4 portions</p>
                </div></a></article>
            <!-- Carousel item-->
            <article class="pb-2"><a class="card h-100 border-0 shadow card-hover mx-1" href="#"><span class="badge badge-lg badge-floating badge-floating-end bg-info">Soups</span>
                <div class="card-img-top card-img-gradient"><img src="/frontend/img/demo/food-blog/featured-recipes/02.jpg" alt="Shrimp Soup With Chili"><span class="card-floating-text text-light fw-medium">Discover this recipe<i class="ai-chevron-right fs-lg ms-1 mt-n1 align-middle"></i></span></div>
                <div class="card-body">
                  <div class="mb-3 text-nowrap"><span class="meta-link fs-xs"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Sep 29</span><span class="meta-divider"></span><span class="meta-link fs-xs"><i class="ai-message-square me-1"></i>&nbsp;1</span></div>
                  <h3 class="h5 pt-1 mb-3">Shrimp Soup With Chili</h3>
                  <p class="fs-sm text-muted mb-2">35 min — 2-4 portions</p>
                </div></a></article>
            <!-- Carousel item-->
            <article class="pb-2"><a class="card h-100 border-0 shadow card-hover mx-1" href="#"><span class="badge badge-lg badge-floating badge-floating-end bg-warning">Desserts</span>
                <div class="card-img-top card-img-gradient"><img src="/frontend/img/demo/food-blog/featured-recipes/03.jpg" alt="Healthy Dessert With Nuts &amp;amp; Berries"><span class="card-floating-text text-light fw-medium">Discover this recipe<i class="ai-chevron-right fs-lg ms-1 mt-n1 align-middle"></i></span></div>
                <div class="card-body">
                  <div class="mb-3 text-nowrap"><span class="meta-link fs-xs"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Sep 12</span><span class="meta-divider"></span><span class="meta-link fs-xs"><i class="ai-message-square me-1"></i>&nbsp;4</span></div>
                  <h3 class="h5 pt-1 mb-3">Healthy Dessert With Nuts &amp; Berries</h3>
                  <p class="fs-sm text-muted mb-2">15 min — 6-8 portions</p>
                </div></a></article>
            <!-- Carousel item-->
            <article class="pb-2"><a class="card h-100 border-0 shadow card-hover mx-1" href="#"><span class="badge badge-lg badge-floating badge-floating-end bg-warning">Desserts</span>
                <div class="card-img-top card-img-gradient"><img src="/frontend/img/demo/food-blog/featured-recipes/04.jpg" alt="Fluffy American pancakes"><span class="card-floating-text text-light fw-medium">Discover this recipe<i class="ai-chevron-right fs-lg ms-1 mt-n1 align-middle"></i></span></div>
                <div class="card-body">
                  <div class="mb-3 text-nowrap"><span class="meta-link fs-xs"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Nov 4</span><span class="meta-divider"></span><span class="meta-link fs-xs"><i class="ai-message-square me-1"></i>&nbsp;2</span></div>
                  <h3 class="h5 pt-1 mb-3">Fluffy American pancakes</h3>
                  <p class="fs-sm text-muted mb-2">10 min — 3-6 portions</p>
                </div></a></article>
          </div>
        </div>
      </section>
      <!-- Book-->
      <section class="container" id="book">
        <div class="row mx-0 rounded-3 overflow-hidden" style="background-color: #f3f3f9;">
          <div class="col-lg-6" style="background-color: #f4f4f6;">
            <div class="d-flex align-items-start flex-sm-row flex-column py-lg-2 py-5 px-sm-5 px-4"><img class="me-sm-5 ms-sm-0 mx-auto mb-sm-0 mb-4" src="/frontend/img/demo/food-blog/book.png" alt="New recipes book">
              <div class="align-self-center ps-2 text-sm-start text-center">
                <h2 class="mb-4">The new vegan recipes book!</h2><a class="btn btn-primary" href="#"><i class="ai-download me-1"></i>Download book</a>
              </div>
            </div>
          </div>
          <div class="col-lg-6 d-flex align-items-center bg-faded-primary">
            <div class="w-100 py-lg-2 py-5 px-sm-5 px-4">
              <h2 class="mb-2 text-center">Subscribe our newsletter</h2>
              <h6 class="mb-4 pb-2 fw-normal text-center">Never miss a post!</h6>
              <!-- Form subscribe-->
              <form class="input-group mx-auto" style="max-width: 600px;">
                <input class="form-control" type="email" placeholder="Enter your email" required>
                <button class="btn btn-success" type="submit"><i class="ai-send fs-xl"></i></button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!-- Categories-->
      <section class="container py-5 my-4 my-sm-0 py-sm-6 py-md-7">
        <h2 class="h1 mb-4 pb-3 text-center">Popular categories</h2>
        <div class="pb-4" data-simplebar data-simplebar-auto-hide="false">
          <div class="d-flex justify-content-between"><a class="nav-link-style" href="#" style="min-width: 120px;"><img class="d-block mx-auto mb-4" src="/frontend/img/demo/food-blog/categories/01.svg" alt="Healthy" style="max-width: 80px;">
              <p class="mb-0 text-center">Healthy</p></a><a class="nav-link-style" href="#" style="min-width: 120px;"><img class="d-block mx-auto mb-4" src="/frontend/img/demo/food-blog/categories/02.svg" alt="Desserts" style="max-width: 80px;">
              <p class="mb-0 text-center">Desserts</p></a><a class="nav-link-style" href="#" style="min-width: 120px;"><img class="d-block mx-auto mb-4" src="/frontend/img/demo/food-blog/categories/03.svg" alt="Vegetarian" style="max-width: 80px;">
              <p class="mb-0 text-center">Vegetarian</p></a><a class="nav-link-style" href="#" style="min-width: 120px;"><img class="d-block mx-auto mb-4" src="/frontend/img/demo/food-blog/categories/04.svg" alt="Seafood" style="max-width: 80px;">
              <p class="mb-0 text-center">Seafood</p></a><a class="nav-link-style" href="#" style="min-width: 120px;"><img class="d-block mx-auto mb-4" src="/frontend/img/demo/food-blog/categories/05.svg" alt="Bakery" style="max-width: 80px;">
              <p class="mb-0 text-center">Bakery</p></a><a class="nav-link-style" href="#" style="min-width: 120px;"><img class="d-block mx-auto mb-4" src="/frontend/img/demo/food-blog/categories/06.svg" alt="Soups" style="max-width: 80px;">
              <p class="mb-0 text-center">Soups</p></a></div>
        </div>
      </section>
      <!-- Posts with sidebar right-->
      <section class="container">
        <div class="row">
          <!-- Content-->
          <div class="col-lg-9">
            <!-- Post-->
            <article class="card card-horizontal card-hover mb-grid-gutter"><a class="card-img-top" href="#" style="background-image: url(/frontend/img/demo/food-blog/posts/01.jpg);"><span class="badge badge-lg badge-floating text-white" style="background-color: #f74f78;">Seafood</span></a>
              <div class="card-body"><span class="d-inline-block mb-2 pb-1 fs-sm text-muted"><i class="ai-clock fs-base me-2 mt-n1"></i>10 min — 6-8 portions</span>
                <h2 class="h4 nav-heading text-capitalize mb-3"><a href="#">Asian chilli garlic shrimps</a></h2>
                <p class="mb-0 fs-sm text-muted">Dignissim suspendisse in est ante in nibh mauris cursus mattis. Tempor id eu nisl nunc mi. Scelerisque eu ultrices vitae auctor. Ut faucibus pulvinar elementum integer. Sed vulputate mi sit amet mauris commodo. In eu mi bibendum neque…</p>
                <div class="mt-3 text-end text-nowrap"><a class="meta-link fs-xs" href="#"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Oct 29</a><span class="meta-divider"></span><a class="meta-link fs-xs" href="#"><i class="ai-message-square me-1"></i>&nbsp;4</a></div>
              </div>
            </article>
            <!-- Post-->
            <article class="card card-horizontal card-hover mb-grid-gutter">
              <div class="card-img-top d-flex align-items-center justify-content-center" style="background-image: url(/frontend/img/demo/food-blog/posts/02.jpg);"><span class="badge badge-lg badge-floating bg-warning">Desserts</span><a class="btn-video btn-video-sm" href="https://www.youtube.com/watch?v=JzJsUW4xV7k&amp;ab_channel=HealthNutNutrition"></a></div>
              <div class="card-body"><span class="d-inline-block mb-2 pb-1 fs-sm text-muted"><i class="ai-video fs-base me-2 mt-n1"></i>12:49</span>
                <h2 class="h4 nav-heading text-capitalize mb-3"><a href="#">Belgian Waffles with Triple Berry Sauce</a></h2>
                <p class="mb-0 fs-sm text-muted">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa…</p>
                <div class="mt-3 text-end text-nowrap"><a class="meta-link fs-xs" href="#"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Oct 17</a><span class="meta-divider"></span><a class="meta-link fs-xs" href="#"><i class="ai-message-square me-1"></i>&nbsp;2</a></div>
              </div>
            </article>
            <!-- Post-->
            <article class="card card-horizontal card-hover mb-grid-gutter"><a class="card-img-top" href="#" style="background-image: url(/frontend/img/demo/food-blog/posts/03.jpg);"><span class="badge badge-lg badge-floating bg-success">Vegetarian</span></a>
              <div class="card-body"><span class="d-inline-block mb-2 pb-1 fs-sm text-muted"><i class="ai-clock fs-base me-2 mt-n1"></i>17 min — 4-6 portions</span>
                <h2 class="h4 nav-heading text-capitalize mb-3"><a href="#">Vegan Bowl with Avocado, Cabbage and Quinoa</a></h2>
                <p class="mb-0 fs-sm text-muted">Find aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Et harum quidem rerum facilis est et expedita distinctio. </p>
                <div class="mt-3 text-end text-nowrap"><a class="meta-link fs-xs" href="#"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Oct 9</a><span class="meta-divider"></span><a class="meta-link fs-xs" href="#"><i class="ai-message-square me-1"></i>&nbsp;3</a></div>
              </div>
            </article>
            <!-- Post-->
            <article class="card card-horizontal card-hover mb-grid-gutter"><a class="card-img-top" href="#" style="background-image: url(/frontend/img/demo/food-blog/posts/04.jpg);"><span class="badge badge-lg badge-floating bg-primary">Healthy</span></a>
              <div class="card-body"><span class="d-inline-block mb-2 pb-1 fs-sm text-muted"><i class="ai-clock fs-base me-2 mt-n1"></i>10 min — 2-4 portions</span>
                <h2 class="h4 nav-heading text-capitalize mb-3"><a href="#">Toast in a Bowl</a></h2>
                <p class="mb-0 fs-sm text-muted">Dui ut ornare lectus sit amet est placerat. Diam sit amet nisl suscipit adipiscing bibendum est ultricies integer. Nunc mattis enim ut tellus elementum sagittis vitae. Gravida dictum fusce ut placerat. Sit amet consectetur adipiscing elit duis…</p>
                <div class="mt-3 text-end text-nowrap"><a class="meta-link fs-xs" href="#"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Oct 2</a><span class="meta-divider"></span><a class="meta-link fs-xs" href="#"><i class="ai-message-square me-1"></i>&nbsp;1</a></div>
              </div>
            </article>
            <!-- Post-->
            <article class="card card-horizontal card-hover mb-grid-gutter"><a class="card-img-top" href="#" style="background-image: url(/frontend/img/demo/food-blog/posts/05.jpg);"><span class="badge badge-lg badge-floating bg-success">Vegetarian</span></a>
              <div class="card-body"><span class="d-inline-block mb-2 pb-1 fs-sm text-muted"><i class="ai-clock fs-base me-2 mt-n1"></i>15 min — 2-4 portions</span>
                <h2 class="h4 nav-heading text-capitalize mb-3"><a href="#">Vegan Stir Fry with Rice Vermicelli</a></h2>
                <p class="mb-0 fs-sm text-muted">Non nisi est sit amet. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Mi bibendum neque egestas congue quisque egestas diam in. Donec adipiscing tristique risus nec similique sunt in culpa…</p>
                <div class="mt-3 text-end text-nowrap"><a class="meta-link fs-xs" href="#"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Sep 15</a><span class="meta-divider"></span><a class="meta-link fs-xs" href="#"><i class="ai-message-square me-1"></i>&nbsp;4</a></div>
              </div>
            </article>
            <!-- Post-->
            <article class="card card-horizontal card-hover mb-grid-gutter">
              <div class="card-img-top d-flex align-items-center justify-content-center" style="background-image: url(/frontend/img/demo/food-blog/posts/06.jpg);"><span class="badge badge-lg badge-floating bg-info">Soups</span><a class="btn-video btn-video-sm" href="https://www.youtube.com/watch?v=JzJsUW4xV7k&amp;ab_channel=HealthNutNutrition"></a></div>
              <div class="card-body"><span class="d-inline-block mb-2 pb-1 fs-sm text-muted"><i class="ai-video fs-base me-2 mt-n1"></i>20:15</span>
                <h2 class="h4 nav-heading text-capitalize mb-3"><a href="#">Spicy Chicken Soup</a></h2>
                <p class="mb-0 fs-sm text-muted">Nulla aliquet enim tortor at auctor. Diam vel quam elementum pulvinar etiam. Non diam phasellus vestibulum lorem sed risus ultricies tristique. Et tortor consequat id porta nibh venenatis cras. Pellentesque id nibh tortor id aliquet lectus…</p>
                <div class="mt-3 text-end text-nowrap"><a class="meta-link fs-xs" href="#"><i class="ai-calendar me-1 mt-n1"></i>&nbsp;Sep 8</a><span class="meta-divider"></span><a class="meta-link fs-xs" href="#"><i class="ai-message-square me-1"></i>&nbsp;1</a></div>
              </div>
            </article>
            <!-- Pagination-->
            <div class="d-md-flex justify-content-between align-items-center pt-3 pb-sm-2">
              <div class="d-flex justify-content-center align-items-center mb-4">
                <label class="pe-1 me-2 mt-n1 fs-sm">Show</label>
                <select class="form-select me-2" style="width: 5rem;">
                  <option value="6">6</option>
                  <option value="12">12</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option value="40">40</option>
                </select>
                <div class="fs-sm text-nowrap ps-1 mb-1">posts per page</div>
              </div>
              <nav class="mb-4" aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                  <li class="page-item"><a class="page-link" href="#" aria-label="Previous"><i class="ai-chevron-left"></i></a></li>
                  <li class="page-item d-sm-none"><span class="page-link page-link-static">2 / 10</span></li>
                  <li class="page-item d-none d-sm-block"><a class="page-link" href="#">1</a></li>
                  <li class="page-item active d-none d-sm-block" aria-current="page"><span class="page-link">2<span class="visually-hidden">(current)</span></span></li>
                  <li class="page-item d-none d-sm-block"><a class="page-link" href="#">3</a></li>
                  <li class="page-item d-none d-sm-block"><a class="page-link" href="#">4</a></li>
                  <li class="page-item d-none d-sm-block">...</li>
                  <li class="page-item d-none d-sm-block"><a class="page-link" href="#">10</a></li>
                  <li class="page-item"><a class="page-link" href="#" aria-label="Next"><i class="ai-chevron-right"></i></a></li>
                </ul>
              </nav>
            </div>
          </div>
          <!-- Sidebar-->
          <aside class="col-lg-3">
            <div class="offcanvas offcanvas-collapse offcanvas-end" id="blog-sidebar">
              <div class="offcanvas-cap navbar-shadow px-4 mb-3">
                <h5 class="mt-1 mb-0">Sidebar</h5>
                <button class="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
              <div class="offcanvas-body px-4 pt-3 pt-lg-0 pe-lg-0 ps-lg-2 ps-xl-4" data-simplebar>
                <!-- Categories-->
                <div class="widget widget-categories mb-5">
                  <h3 class="widget-title">Ingredients</h3>
                  <ul>
                    <li><a class="widget-link" href="#">Apple<small class="text-muted ps-1 ms-2">23</small></a></li>
                    <li><a class="widget-link" href="#">Banana<small class="text-muted ps-1 ms-2">14</small></a></li>
                    <li><a class="widget-link" href="#">Berries<small class="text-muted ps-1 ms-2">7</small></a></li>
                    <li><a class="widget-link" href="#">Cabbage<small class="text-muted ps-1 ms-2">19</small></a></li>
                    <li><a class="widget-link" href="#">Citrus<small class="text-muted ps-1 ms-2">26</small></a></li>
                    <li><a class="widget-link" href="#">Corn<small class="text-muted ps-1 ms-2">8</small></a></li>
                    <li><a class="widget-link" href="#">Cucumber<small class="text-muted ps-1 ms-2">25</small></a></li>
                    <li><a class="widget-link" href="#">Egg<small class="text-muted ps-1 ms-2">14</small></a></li>
                    <li><a class="widget-link" href="#">Herbs<small class="text-muted ps-1 ms-2">27</small></a></li>
                    <li><a class="widget-link" href="#">Potato<small class="text-muted ps-1 ms-2">29</small></a></li>
                    <li><a class="widget-link" href="#">Tomatoes<small class="text-muted ps-1 ms-2">32</small></a></li>
                    <li><a class="widget-link" href="#">Watermelon<small class="text-muted ps-1 ms-2">4</small></a></li>
                  </ul>
                </div>
                <!-- Featured posts-->
                <div class="widget mt-n1 mb-5">
                  <h3 class="widget-title pb-1">Popular recipes</h3>
                  <div class="d-flex align-items-start pb-1 mb-3"><a class="d-block flex-shrink-0" href="#"><img class="rounded" src="/frontend/img/demo/food-blog/posts/th/01.jpg" alt="Post" width="64"></a>
                    <div class="ps-2 ms-1">
                      <h4 class="fs-md nav-heading mb-1"><a class="fw-medium" href="#">Sweet Cream Cupcake</a></h4>
                      <p class="fs-xs text-muted mb-0">by Amelie Mayer</p>
                    </div>
                  </div>
                  <div class="d-flex align-items-start pb-1 mb-3"><a class="d-block flex-shrink-0" href="#"><img class="rounded" src="/frontend/img/demo/food-blog/posts/th/02.jpg" alt="Post" width="64"></a>
                    <div class="ps-2 ms-1">
                      <h4 class="fs-md nav-heading mb-1"><a class="fw-medium" href="#">Keto Paleo Carb-Free Breakfast</a></h4>
                      <p class="fs-xs text-muted mb-0">by Amelie Mayer</p>
                    </div>
                  </div>
                  <div class="d-flex align-items-start pb-1 mb-3"><a class="d-block flex-shrink-0" href="#"><img class="rounded" src="/frontend/img/demo/food-blog/posts/th/03.jpg" alt="Post" width="64"></a>
                    <div class="ps-2 ms-1">
                      <h4 class="fs-md nav-heading mb-1"><a class="fw-medium" href="#">Sugar-Free Coconut Yogurt with Mango</a></h4>
                      <p class="fs-xs text-muted mb-0">by Amelie Mayer</p>
                    </div>
                  </div>
                </div>
                <!-- Tag cloud-->
                <div class="widget mb-5">
                  <h3 class="widget-title pb-1">Popular tags</h3><a class="btn-tag me-2 mb-2" href="#">#bakery</a><a class="btn-tag me-2 mb-2" href="#">#cookbook</a><a class="btn-tag me-2 mb-2" href="#">#cuisine</a><a class="btn-tag me-2 mb-2" href="#">#asian food</a><a class="btn-tag me-2 mb-2" href="#">#tips</a><a class="btn-tag me-2 mb-2" href="#">#recipe</a><a class="btn-tag me-2 mb-2" href="#">#chef</a><a class="btn-tag me-2 mb-2" href="#">#vegetarian</a>
                </div>
                <!-- Socials-->
                <div class="mb-5">
                  <h3 class="widget-title pb-1">Follow me</h3><a class="btn-social bs-outline bs-facebook me-2 mb-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-outline bs-twitter me-2 mb-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-outline bs-instagram me-2 mb-2" href="#"><i class="ai-instagram"></i></a><a class="btn-social bs-outline bs-pinterest me-2 mb-2" href="#"><i class="ai-pinterest"></i></a>
                </div>
              </div>
            </div>
          </aside>
        </div>
      </section>
      <!-- Videos-->
      <section class="container py-5 mb-4 my-sm-0 py-sm-6 py-md-7">
        <h2 class="h1 text-center">New video recipes every Sunday</h2>
        <p class="mx-auto mb-5 text-muted text-center" style="max-width: 600px;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa quae</p>
        <!-- Carousel-->
        <div class="tns-carousel-wrapper">
          <div class="tns-carousel-inner" data-carousel-options="{&quot;navAsThumbnails&quot;: true, &quot;controls&quot;: false, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1, &quot;gutter&quot;: 16},&quot;768&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 30}}}">
            <!-- Carousel item-->
            <div>
              <div class="gallery"><a class="gallery-item gallery-video rounded-3" href="https://www.youtube.com/watch?v=JzJsUW4xV7k&amp;ab_channel=HealthNutNutrition" data-sub-html="&lt;h6 class=&quot;fs-sm text-light&quot;&gt;Gallery video caption&lt;/h6&gt;"><img src="/frontend/img/demo/food-blog/videos/01.jpg" alt="Gallery thumbnail"><span class="gallery-caption">Gallery video caption</span></a></div>
            </div>
            <!-- Carousel item-->
            <div>
              <div class="gallery"><a class="gallery-item gallery-video rounded-3" href="https://www.youtube.com/watch?v=JzJsUW4xV7k&amp;ab_channel=HealthNutNutrition" data-sub-html="&lt;h6 class=&quot;fs-sm text-light&quot;&gt;Gallery video caption&lt;/h6&gt;"><img src="/frontend/img/demo/food-blog/videos/02.jpg" alt="Gallery thumbnail"><span class="gallery-caption">Gallery video caption</span></a></div>
            </div>
            <!-- Carousel item-->
            <div>
              <div class="gallery"><a class="gallery-item gallery-video rounded-3" href="https://www.youtube.com/watch?v=JzJsUW4xV7k&amp;ab_channel=HealthNutNutrition" data-sub-html="&lt;h6 class=&quot;fs-sm text-light&quot;&gt;Gallery video caption&lt;/h6&gt;"><img src="/frontend/img/demo/food-blog/videos/03.jpg" alt="Gallery thumbnail"><span class="gallery-caption">Gallery video caption</span></a></div>
            </div>
          </div>
        </div>
      </section>
      <!-- About-->
      <section class="container">
        <div class="row align-items-center">
          <div class="col-md-5 mb-md-0 mb-grid-gutter"><img class="rounded-3" src="/frontend/img/demo/food-blog/author.jpg" alt="Amelie"></div>
          <div class="col-lg-6 offset-lg-1 col-md-7">
            <h2 class="mb-2">Hi! I am Amelie</h2>
            <h3 class="h6 mb-3 text-muted">Amateur chef and creator of this blog</h3>
            <p class="mb-4 pb-2 text-muted">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p><a class="btn btn-primary" href="#">Get to know me better</a>
          </div>
        </div>
      </section>
      <!-- Instagram-->
      <section class="container-fluid py-5 my-4 my-sm-0 pt-sm-6 pt-md-7">
        <h2 class="mb-5 text-center">Follow <a href='#' class='d-inline-block p-2 bg-faded-primary rounded text-primary text-decoration-none'>@amelie_chef</a> on Instagram</h2>
        <!-- Carousel-->
        <div class="tns-carousel-wrapper">
          <div class="tns-carousel-inner" data-carousel-options="{&quot;navAsThumbnails&quot;: true, &quot;controls&quot;: false, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 15}, &quot;576&quot;:{&quot;items&quot;:3}, &quot;768&quot;:{&quot;items&quot;:4, &quot;gutter&quot;: 30}, &quot;992&quot;:{&quot;items&quot;:5}, &quot;1200&quot;:{&quot;items&quot;:6}}}">
            <!-- Carousel item-->
            <div><img src="/frontend/img/demo/food-blog/instagram/01.jpg" alt="01"></div>
            <!-- Carousel item-->
            <div><img src="/frontend/img/demo/food-blog/instagram/02.jpg" alt="02"></div>
            <!-- Carousel item-->
            <div><img src="/frontend/img/demo/food-blog/instagram/03.jpg" alt="03"></div>
            <!-- Carousel item-->
            <div><img src="/frontend/img/demo/food-blog/instagram/04.jpg" alt="04"></div>
            <!-- Carousel item-->
            <div><img src="/frontend/img/demo/food-blog/instagram/05.jpg" alt="05"></div>
            <!-- Carousel item-->
            <div><img src="/frontend/img/demo/food-blog/instagram/06.jpg" alt="06"></div>
          </div>
        </div>
      </section>


</div>  


@push('styles')
	<link rel="stylesheet" media="screen" href="/frontend/vendor/tiny-slider/dist/tiny-slider.css">
    <link rel="stylesheet" media="screen" href="/frontend/vendor/lightgallery.js/dist/css/lightgallery.min.css">
@endpush

@push('scripts')
	
	<script src="/frontend/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
    <script src="/frontend/vendor/lightgallery.js/dist/js/lightgallery.min.js"></script>
    <script src="/frontend/vendor/lg-video.js/dist/lg-video.min.js"></script>

@endpush
