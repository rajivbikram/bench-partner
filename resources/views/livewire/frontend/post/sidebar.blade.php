<div class="sidebar col-lg-3 pt-lg-5">
  <div class="offcanvas offcanvas-collapse" id="blog-sidebar">
    <div class="offcanvas-cap navbar-shadow px-4 mb-3">
      <h5 class="mt-1 mb-0">Chapters</h5>
      <button class="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body px-4 pt-3 pt-lg-0 ps-lg-0 pe-lg-2 pe-xl-4" data-simplebar>
      <div class="simplebar-content">
      <!-- Search-->
      <div class="widget mb-5">
        <div class="input-group"><i class="ai-search position-absolute top-50 start-0 translate-middle-y ms-3"></i>
          <input class="form-control rounded" type="text" placeholder="Search">
        </div>
      </div>
      <!-- Widget: Collapsible categories -->
      <div class="widget widget-categories mt-n1 mb-5">
        @if ($subjectSlug)
          <h3 class="widget-title">
            <a href="{{ route('frontend.subject.details',$subjectSlug) }}">{{ $subjectName }}</a>
          </h3>
        @endif
        
        <ul id="chapters">
          @if ($chapters)
            @foreach ($chapters as $chapter)
              <li>
                <a class="widget-link @if ($chapterId != $chapter->id) collapsed @endif" href="#{{ $chapter->slug.'-'.$chapter->id }}" data-bs-toggle="collapse" aria-expanded="@if ($chapterId == $chapter->id) true @endif">
                  {{ $chapter->name }}
                </a>
                  <ul class="collapse @if ($chapterId == $chapter->id) show @endif" id="{{ $chapter->slug.'-'.$chapter->id }}" data-bs-parent="#chapters">
                    @if (count($chapter->posts)>0)
                      @foreach ($chapter->posts as $chapterPost)
                        <li>
                          <a class="widget-link @if ($chapterPost->id == $postId) active @endif" href="{{ route('frontend.post.details',$chapterPost->post_slug) }}">
                            {{ $chapterPost->post_short_title ?? $chapterPost->post_title }}
                          </a>
                        </li>
                      @endforeach
                    @else
                      <p class="mb-0 fs-sm text-muted">No Posts</p>
                    @endif
                  </ul>
              </li>
            @endforeach
          @endif
        </ul>
      </div>
    </div>
  </div>
</div>
</div>