@section('title', $postTitle)
<div>

  <div class="sidebar-enabled">
    <div class="container">
      <div class="row">
        <!-- Sidebar-->
        @include('livewire.frontend.post.sidebar')
        <!-- Content-->
        <div class="col-lg-9 content py-4 mb-2 mb-sm-0 pb-sm-5">
          <div class="pb-4">
            <nav aria-label="breadcrumb">
              <ol class="py-1 my-2 breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="ai-home"></i> Home</a></li>
                @if (!empty($subjectName))
                  <li class="breadcrumb-item">
                    <a href="{{ route('frontend.subject.details',$subjectSlug ) }}">{{ $subjectName }}</a>
                  </li> 
                @endif
                <li class="breadcrumb-item active" aria-current="page">{{ $postTitle }} </li>
              </ol>
            </nav>
            <h1>{{ $postTitle }}</h1>
          </div>
          <!-- Post author + Sharing-->
          <div class="row position-relative g-0 align-items-center border-top border-bottom mb-4">
            <div class="col-md-6 py-2 pe-md-3">
              <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                <div class="d-flex align-items-center me-grid-gutter"><a class="d-block" href="#"><img class="rounded-circle me-1" src="/frontend/img/blog/single/author.jpg" alt="{{ $createdBy }}" width="50"></a>
                  <div class="ps-2">
                    <h6 class="nav-heading mb-0"><a href="#">{{ $createdBy }}</a></h6>
                    <div class="text-nowrap">
                      <div class="meta-link fs-xs" datetime="{{ $seoUpdatedAt }}">
                        <i class="ai-calendar me-1 align-vertical"></i>&nbsp;
                        {{ $updatedAt }}
                      </div>
                      @if ($commentStatus == 'open')
                        <span class="meta-divider"></span>
                        <a class="meta-link fs-xs" href="#comments" data-scroll="">
                          <i class="ai-message-square me-1 align-vertical"></i>&nbsp;3
                        </a>
                      @endif
                    </div>
                  </div>
                </div>
                <a class="btn btn-translucent-primary btn-sm me-2" href="#">Follow</a>
                @auth
                  <a class="btn btn-translucent-success btn-sm" href="{{ route('backend.post.edit', [$username, $postId]) }}">Edit</a>
                @endauth
              </div>
            </div>
            <div class="d-none d-md-block position-absolute border-start h-100" style="top: 0; left: 50%; width: 1px;"></div>
            <div class="col-md-6 d-none d-sm-block ps-md-3 py-2">
              <div class="d-flex align-items-center justify-content-center justify-content-md-end">
                <h6 class="text-nowrap my-2 me-3">Share:</h6><a class="btn-social bs-outline bs-facebook ms-2 my-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-outline bs-twitter ms-2 my-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-outline bs-google ms-2 my-2" href="#"><i class="ai-google"></i></a><a class="btn-social bs-outline bs-email ms-2 my-2" href="#"><i class="ai-mail"></i></a>
              </div>
            </div>
          </div>
          <!-- Post content-->
          <div class="post-content">
            {!! $postContent !!}  
          </div>
          <!-- Helpful + Sharing-->
          <div class="row g-0 position-relative align-items-center border-top border-bottom my-5">
            <div class="col-md-6 py-2 py-dm-3 pe-md-3 d-flex align-items-center justify-content-center justify-content-md-start">
              <h3 class="h5 my-2 pe-sm-2 me-4">Did you find this is helpful?</h3>
              <div class="text-nowrap py-2">
                <button class="btn-like" wire:click="like" type="button">{{ $likeCount }}</button>
                <button class="btn-dislike" wire:click="dislike" type="button">{{ $dislikeCount }}</button>
              </div>
            </div>
            <div class="d-none d-md-block position-absolute border-start h-100" style="top: 0; left: 50%; width: 1px;"></div>
            <div class="col-md-6 ps-md-3 py-2 py-md-3">
              <div class="d-flex align-items-center justify-content-center justify-content-md-end">
                <button class="btn btn-translucent-danger rounded-pill btn-sm" wire:click="thanks">
                  <i class="ai-heart"></i> Thanks 
                  @if ($thankCount)
                    <span class="me-2 fs-sm">({{ $thankCount }})</span>
                  @endif
                </button>
              </div>
            </div>
          </div>
          <!-- Prev / Next post navigation-->
          <nav class="d-flex justify-content-between pb-4 mb-5" aria-label="Entry navigation">
            @if (!empty($previousPost))
            <a class="entry-nav me-3" href="{{ route('frontend.post.details', $previousPost->post_slug) }}">
              <h3 class="h5 pb-sm-2">Prev post</h3>
              <div class="d-flex align-items-start">
                <div class="entry-nav-thumb flex-shrink-0 d-none d-sm-block">
                  <img class="rounded" src="/frontend/img/blog/th04.jpg" alt="Post thumbnail">
                </div>
                <div class="ps-sm-3">
                  <h4 class="nav-heading fs-md fw-medium mb-0">{{ $previousPost->post_title }}</h4>
                </div>
              </div>
            </a>
            @endif
            @if (!empty($nextPost))
              <a class="entry-nav ms-3" href="{{ route('frontend.post.details', $nextPost->post_slug) }}">
                <h3 class="h5 pb-sm-2 text-end">Next post</h3>
                <div class="d-flex align-items-start">
                  <div class="text-end pe-sm-3">
                    <h4 class="nav-heading fs-md fw-medium mb-0">{{ $nextPost->post_title }}</h4>
                  </div>
                  <div class="entry-nav-thumb flex-shrink-0 d-none d-sm-block">
                    <img class="rounded" src="/frontend/img/blog/th05.jpg" alt="Post thumbnail">
                  </div>
                </div>
              </a>
            @endif
            </nav>
            @if ($commentStatus == 'open')
                <!-- Comments-->
                <div class="pb-4 mb-5" id="comments">
                  <h2 class="h3 pb-4">3 Comments</h2>
                  <div class="comment">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat cumque nihil impedit quo minus. Duis aute irure dolor in reprehenderit in voluptate.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="d-flex align-items-center"><img class="rounded-circle" src="/frontend/img/blog/comments/01.jpg" alt="Barbara Palson" width="42">
                        <div class="ps-2 ms-1">
                          <h4 class="fs-sm mb-0">Barbara Palson</h4><span class="fs-xs text-muted">3 days ago</span>
                        </div>
                      </div><a class="btn btn-outline-primary btn-sm" href="#"><i class="ai-corner-up-left fs-base me-2 ms-n1"></i>Reply</a>
                    </div>
                    <div class="comment">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat cumque nihil impedit quo minus. Duis aute irure dolor in reprehenderit in voluptate.</p>
                      <div class="d-flex align-items-center"><img class="rounded-circle" src="/frontend/img/blog/comments/02.jpg" alt="Daniel Adams" width="42">
                        <div class="ps-2 ms-1">
                          <h4 class="fs-sm mb-0">Daniel Adams</h4><span class="fs-xs text-muted">2 days ago</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment">
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="d-flex align-items-center"><img class="rounded-circle" src="/frontend/img/blog/comments/03.jpg" alt="Tim Brooks" width="42">
                        <div class="ps-2 ms-1">
                          <h4 class="fs-sm mb-0">Tim Brooks</h4><span class="fs-xs text-muted">1 week ago</span>
                        </div>
                      </div><a class="btn btn-outline-primary btn-sm" href="#"><i class="ai-corner-up-left fs-base me-2 ms-n1"></i>Reply</a>
                    </div>
                  </div><a class="btn btn-translucent-primary d-block w-100" href="#comment-form" data-bs-toggle="collapse">Join the conversation</a>
                  <!-- Comment form-->
                  <div class="collapse" id="comment-form">
                    <form class="needs-validation bg-light rounded-3 shadow p-4 p-lg-5 mt-4" novalidate="">
                      <div class="row">
                        <div class="col-sm-6 mb-3">
                          <label class="form-label" for="com-name">Your name<sup class="text-danger ms-1">*</sup></label>
                          <input class="form-control" type="text" id="com-name" placeholder="Enter your name" required="">
                          <div class="invalid-feedback">Please enter your name.</div>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-sm-6 mb-3">
                          <label class="form-label" for="com-email">Email address<sup class="text-danger ms-1">*</sup></label>
                          <input class="form-control" type="email" id="com-email" placeholder="Enter your email" required="">
                          <div class="invalid-feedback">Please provide a vild email address.</div>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="mb-3">
                        <label class="form-label" for="com-text">Comment<sup class="text-danger ms-1">*</sup></label>
                        <textarea class="form-control" id="com-text" rows="6" placeholder="Write your comment here" required=""></textarea>
                        <div class="invalid-feedback">Please write your comment.</div>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                      <button class="btn btn-primary" type="submit">Post comment</button>
                    </form>
                  </div>
                </div>
            @endif
          
        </div>
      </div>
    </div>
  </div>

</div> 


@push('seo')

  {{-- SEO Details --}}
  <meta name="description" content="{{ $metaDescription }}"/>
  <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large"/>
  <link rel="canonical" href="{{ $postPermalink }}" />
  <meta property="og:locale" content="en_US">
  <meta property="og:type" content="article">
  <meta property="og:title" content="{{ $postTitle }}">
  <meta property="og:description" content="{{ $metaDescription }}">
  <meta property="og:url" content="{{ $postPermalink }}">
  <meta property="og:site_name" content="Bench Partner">
  <meta property="article:section" content="{{ $subjectName ?? 'No Subject'}}">
  <meta property="og:updated_time" content="{{ $seoUpdatedAt }}">
  <meta property="og:image" content="https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png">
  <meta property="og:image:secure_url" content="https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png">
  <meta property="og:image:width" content="466">
  <meta property="og:image:height" content="526">
  <meta property="og:image:alt" content="{{ $postTitle }}">
  <meta property="og:image:type" content="image/png">
  <meta property="article:published_time" content="2019-07-06GMT+000017:46:18+00:00">
  <meta property="article:modified_time" content="2020-10-19GMT+000018:11:21+00:00">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="{{ $postTitle }}">
  <meta name="twitter:description" content="{{ $metaDescription }}">
  <meta name="twitter:image" content="https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png">
  <script type="application/ld+json" class="rank-math-schema">
  {"@context":"https://schema.org",
  "@graph":[{
  "@type":["Person","Organization"],
  "@id":"https://benchpartner.com/#person",
  "name":"Bench Partner",
  "logo":{"@type":"ImageObject","url":"https://benchpartner.com/wp-content/uploads/2020/07/logo.png"},
  "image":{"@type":"ImageObject","url":"https://benchpartner.com/wp-content/uploads/2020/07/logo.png"}},
  {"@type":
  "WebSite","@id":"https://benchpartner.com/#website","url":"https://benchpartner.com","name":"Bench Partner",
  "publisher":{"@id":"https://benchpartner.com/#person"},"inLanguage":"en-US"},
  {"@type":"ImageObject","@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#primaryImage","url":"https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png","width":"466","height":"526"},

  {"@type":"WebPage","@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#webpage","url":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/","name":"{{ $postTitle }}","datePublished":"2019-07-06T17:46:18+00:00","dateModified":"2020-10-19T18:11:21+00:00","isPartOf":{"@id":"https://benchpartner.com/#website"},"primaryImageOfPage":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#primaryImage"},"inLanguage":"en-US"},

  {"@type":"BlogPosting","headline":"{{ $postTitle }}","datePublished":"2019-07-06GMT+000017:46:18+00:00","dateModified":"2020-10-19GMT+000018:11:21+00:00","author":

  {"@type":"Person","name":"Bench Partner"},"description":"{{ $metaDescription }}","@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#schema-10821","isPartOf":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#webpage"},

  "publisher":{"@id":"https://benchpartner.com/#person"},"image":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#primaryImage"},"inLanguage":"en-US","mainEntityOfPage":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#webpage"}}]}

  </script>
  
@endpush


