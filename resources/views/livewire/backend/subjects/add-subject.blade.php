@section('title', 'Create new Subject')
<div>
    <div class="position-relative bg-gradient" style="height: 480px;">
        <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
            <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
          </svg>
        </div>
      </div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -400px;">
        <div class="row">
          <!-- Sidebar-->
           @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->

          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title + Delete link-->
                <div class="d-sm-flex align-items-center justify-content-between pb-2 text-center text-sm-start">
                  <h1 class="h3 mb-1 text-nowrap">Create new Subject</h1>
                  <a class="btn btn-translucent-success btn-sm" href="{{ route('backend.subject.index',$username) }}">
                    <i class="ai-eye me-2"></i> View Subject</a>
                </div>
                <div class="pb-2">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{ route('home') }}">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item">
                        <a href="{{ route('backend.subject.index',$username) }}">
                          Subject
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                  </nav>
                </div>
                @include('layouts.frontend.partials.flash-message')
                <!-- Content-->
                <form class="needs-validation" wire:submit.prevent="store" novalidate enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="mb-4 mt-4">
                          <label class="form-label px-0" for="name">Subject Name <span class="required">*</span></label>
                          <input class="form-control @error('name') is-invalid @enderror" type="text" id="name" wire:model="name" placeholder="Subject Name" required>
                          <div class="invalid-feedback">Subject name is Required.</div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="mb-4">
                          <label class="form-label px-0" for="name">Subject Slug <span class="required">*</span></label>
                          <input class="form-control @error('slug') is-invalid @enderror" type="text" id="slug" placeholder="Post slug" wire:model.defer="slug" required>
                          <div class="invalid-feedback">Subject slug is Required.</div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="mb-3 pb-1">
                            <label class="form-label px-0" for="name">Description</label>
                            <textarea class="form-control" id="description" wire:model.defer="description" placeholder="Description"></textarea>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="mb-3 pb-1">
                          <label class="form-label px-0" for="order">Order</label>
                          <input class="form-control" type="text" id="order" wire:model.defer="order" placeholder="Display Order">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="mb-3 pb-1">
                          <label class="form-label px-0" for="color">Color</label>
                          <input class="form-control" type="text" id="color" wire:model.defer="color" placeholder="color">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="mb-3 pb-2">
                          <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input" wire:model.defer="status" id="status" checked>
                            <label class="form-check-label" for="status">Publish</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="mb-3 pb-2">
                          <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input" wire:model.defer="menu" id="menu">
                            <label class="form-check-label" for="menu">Show in Menu</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="mb-3 pb-1">
                          <label class="form-label px-0" for="view">View</label>
                          <input class="form-control" type="text" id="view" wire:model.defer="view" placeholder="View">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="mb-3 pb-1">
                          <label class="form-label px-0" for="icon">Icon</label>
                          <input class="form-control" type="text" id="icon" wire:model.defer="icon" placeholder="Icon">
                        </div>
                      </div>
                      
                      <div class="col-sm-12">
                        <div class="mb-4 pb-1">
                          @if ($image)
                            <div class="text-center mt-2">
                              <img class="img-thumbnail rounded" src="{{ $image->temporaryUrl() }}" width="100">
                            </div>
                          @endif
                          <label class="form-label px-0" for="image">Subject Image</label>
                          <input class="form-control @error('image') is-invalid @enderror" id="image" wire:model.defer="image" type="file"  accept="image/*">
                        </div>
                      </div>
                      <div class="widget">
                        <h3 class="widget-title">Seo Details</h3>
                        <hr class="mt-2 mb-4">
                      </div>
                      <div class="mb-4">
                        <label class="form-label px-0" for="meta-keyword">Meta Keyword</label>
                        <textarea class="form-control" wire:model.defer="metaKeyword" style="height: 80px;" rows="1" placeholder="Meta Keyword"></textarea>
                      </div>
                      <div class="mb-4">
                        <label class="form-label px-0" for="meta-description">Meta Description</label>
                        <textarea class="form-control" wire:model.defer="metaDescription" style="height: 130px;" rows="4" placeholder="Meta Description"></textarea>
                      </div>
                      <div class="mb-4">
                        @if ($seoImage)
                          <div class="text-center mt-2">
                            <img class="img-thumbnail rounded" src="{{ $seoImage->temporaryUrl() }}" width="100">
                          </div>
                        @endif
                        <label class="form-label px-0" for="seo-image">SEO Image</label>
                        <input class="form-control @error('seoImage') is-invalid @enderror" id="seo-image" wire:model.defer="seoImage" type="file"  accept="image/*">
                      </div>
                      <div class="col-12">
                        <hr class="mt-2 mb-4">
                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                          <a class="btn btn-translucent-primary btnPrevious">
                              <i class="ai-arrow-left"></i> Back
                          </a>
                          <button class="btn btn-primary mt-3 mt-sm-0" type="submit">
                            <span wire:loading.remove wire:target="store"><i class="ai-save fs-lg me-2"></i> Save</span>
                            <span wire:loading wire:target="store"> 
                              <span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span> 
                              Saving...
                            </span>
                          </button>
                        </div>
                      </div>
                    </div>
              </form>
              </div>
            </div>
          </div>

          </div>
        </div>

        


</div>

@push('styles')

@endpush

@push('scripts')

@endpush

@push('seo')
  
@endpush