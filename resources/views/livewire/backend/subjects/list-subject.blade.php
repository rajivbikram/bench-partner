@section('title', 'Subjects')
<div>
    <div class="position-relative bg-gradient" style="height: 480px;">
        <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
            <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
          </svg>
        </div>
      </div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -400px;">
        <div class="row">
          <!-- Sidebar-->
          @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->
          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title + Delete link-->
                <div class="d-sm-flex align-items-center justify-content-between text-center text-sm-start">
                  <h1 class="h3 mb-2 text-nowrap">Subjects</h1>
                  <a class="btn btn-translucent-primary" href="{{ route('backend.subject.create',$username) }}">
                    <i class="ai-plus me-2"></i> Add Subject</a>
                </div>
                <div class="pb-4">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{ route('home') }}">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Subjects</li>
                    </ol>
                  </nav>
                </div>
                @include('layouts.frontend.partials.flash-message')
                <!-- Content-->
                <div class="content">
                    @if (count($subjects)>0)
                    <div class="table-responsive">
                        <table class="table table-hover">
                          <thead;>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Slug</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($subjects as $subject)
                            <tr>
                            <th scope="row">{{ $loop->index + 1 }}</th>
                              <td>{{ $subject->name }}</td>
                              <td>{{ $subject->slug }}</td>
                              <td>
                                  <div class="bg-faded-success text-success fs-xs fw-medium py-1 text-center rounded-1 my-1">{{ $subject->status }}</div>
                              </td>
                              <td>
                                <div class="fs-sm me-2">
                                    <a class="text-primary me-2" href="{{ route('backend.subject.edit',[$username, $subject->id]) }}" title="Edit"><i class="ai-edit"></i> </a>

                                    <a class="text-success me-2" href="#" data-bs-toggle="modal"  data-bs-target="#addModal" title="View"><i class="ai-eye"></i> </a>
                            
                                    <a class="text-danger" wire:click.prevent="deleteId({{ $subject->id }})" title="Delete"  href="#"><i class="ai-trash-2"></i> </a>
                                </div>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                      <nav class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-3">
                        <div class="d-md-flex align-items-center">
                           <span class="fs-sm text-muted me-md-3">Showing {{ $subjects->firstItem() }} to {{ $subjects->lastItem() }}
                            of total {{ $subjects->total() }} entries</span>
                         </div>
                         {{ $subjects->links() }}
                       </nav>
                	@else
                    <div class="card border-0 mt-2 text-center">
                      <div class="card-body">
                      	<img src="/frontend/img/add-new.svg" alt="Add New">
                        <h4 class="card-title mt-3">Create new subject</h4>
                        <p class="card-text">Create new subject to hit the button below</p>
                          <a class="btn btn-primary" wire:click="create" data-bs-toggle="modal"  data-bs-target="#addModal" href="#"><i class="ai-plus me-2"></i> Create subject</a>
                      </div>
                    </div>
                  @endif
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        @include('layouts.frontend.partials.delete-modal')

</div>  


@push('styles')

@endpush

@push('scripts')

<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
   window.addEventListener('delete-conformation', event => {
    Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.isConfirmed) {
        Livewire.emit('deleteConfirmed')
      }
    })
  })
</script>

@endpush
