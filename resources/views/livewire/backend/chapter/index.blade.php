@section('title', 'Chapters')
<div>
  <div class="position-relative bg-gradient" style="height: 400px;">
    <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
      </svg>
    </div>
  </div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -350px;">
        <div class="row">
          <!-- Sidebar-->
           @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->
          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title + Delete link-->
                <div class="d-sm-flex align-items-center justify-content-between text-center text-sm-start">
                  <h1 class="h3 mb-2 text-nowrap">Chapters</h1>
                  <a class="btn btn-translucent-primary mb-2" wire:click="create" data-bs-toggle="modal"  data-bs-target="#addModal">
                    <i class="ai-plus me-2"></i> Add Chapter</a>
                </div>
                <div class="pb-4">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="#">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Chapters</li>
                    </ol>
                  </nav>
                </div>
                @include('layouts.frontend.partials.flash-message')
                <!-- Content-->
                <div class="content">
                  <div class="d-sm-flex justify-content-between mb-3">
                      <input class="form-control" type="text" wire:model="search" placeholder="Search Chapters">
                  </div>
                  @if (count($chapters)>0)
                    <div class="table-responsive fs-md">
                          @foreach ($chapters as $chapter)
                            <div class="accordion" id="orders-accordion">
                              <div class="accordion-item">
                                <h2 class="accordion-header" id="chapter-header-{{ $chapter->id }}">
                                  <div class="accordion-button collapsed accordion-button no-indicator d-flex flex-wrap align-items-center justify-content-between pe-4" type="button" data-bs-toggle="collapse" data-bs-target="#chapter-collapse-{{ $chapter->id }}" aria-expanded="false" aria-controls="chapter-collapse-{{ $chapter->id }}">
                                    <div class="fs-sm text-nowrap my-1 me-2"><i class="ai-hash fs-base me-1"></i><span class="d-inline-block align-middle">{{ $chapter->name }}</span></div>
                                    <div class="text-nowrap text-body fw-medium fs-sm my-1 me-2"></i>{{ $chapter->slug }}</div>
                                    <div class="fs-xs fw-medium py-1 px-3 my-1 me-2">{{ $chapter->subject->name ?? 'No Subject' }}</div>
                                    <div class="text-body fs-sm my-1">
                                      <a class="nav-link-style me-2 text-primary" wire:click.defer="edit({{ $chapter->id }})" href="#" data-bs-toggle="modal"  data-bs-target="#addModal" title="Edit"><i class="ai-edit"></i></a>
                                      <a class="nav-link-style text-danger" title="Delete" wire:click.prevent="deleteId({{ $chapter->id }})" href="#">
                                      <div class="ai-trash-2"></div></a>
                                    </div>
                                  </div>
                                </h2>
                              </div>
                            </div>
                          @endforeach
                          <nav class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-3">
                            {{ $chapters->links() }}
                          </nav>
                    </div>
                  @else
                    <div class="card border-0 mt-2 text-center">
                      <div class="card-body">
                        <img src="/frontend/img/add-new.svg" alt="Add New">
                        <h4 class="card-title mt-3">Create new Chapter</h4>
                        <p class="card-text">Create new role to hit the button below</p>
                          <a class="btn btn-primary" wire:click="create" data-bs-toggle="modal"  data-bs-target="#addModal" href="#"><i class="ai-plus me-2"></i> Create Chapter</a>
                      </div>
                    </div>
                  @endif
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>


    {{-- Add Model --}}
    <div wire:ignore.self class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content border-0">
            <div class="view show" id="modal-signup-view">
              <div class="modal-header px-4">
                <h4 class="modal-title">Add Chapter</h4>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="btn-close"></button>
              </div>
              <div class="modal-body px-4">
                <form wire:submit.prevent="store">
                  <div class="mb-3">
                      <input type="text" wire:model="name" 
                           id="chapterName" 
                           class="form-control @error('name') is-invalid @enderror" 
                           placeholder="Chapter Name"
                           autofocus>
                          @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                  </div>
                  <div class="mb-3">
                      <input type="text" 
                           wire:model="slug" 
                           placeholder="Slug" 
                           class="form-control @error('name') is-invalid @enderror"
                           id="chapterSlug">
                           @error('slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                  </div>
                  <div class="mb-3">
                    <select class="form-select" wire:model.defer="subjectId" id="validationCustom03" required>
                      <option value="">Choose subject...</option>
                      @foreach ($subjects as $subject)
                        <option value="{{ $subject->id }}"
                          @if ($subjectId == $subject->id)
                            selected 
                          @endif
                          >{{ $subject->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="mb-3">
                      <input type="text" 
                           wire:model.defer="order" 
                           placeholder="Order" 
                           class="form-control"
                           id="chapterSlug">
                  </div>
                  
                  <button class="btn btn-danger" data-bs-dismiss="modal" aria-label="btn-close">
                          Cancel
                  </button>
                  <button class="btn btn-primary" type="submit" wire:click="$emit('roleCreated')">
                    <span wire:loading.remove><i class="ai-save fs-lg me-2"></i> Save</span>
                    <span wire:loading> 
                      <span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span> 
                      Saving...
                    </span>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     
      @include('layouts.frontend.partials.delete-modal')

</div>  


@push('styles')

@endpush

@push('scripts')

<script>
    window.addEventListener('closeModel', event => {
    var myModal = document.getElementById('addModal')
    var modal = bootstrap.Modal.getInstance(myModal) 
    modal.hide()
  })
</script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script>
     window.addEventListener('delete-conformation', event => {
      Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
      if (result.isConfirmed) {
          Livewire.emit('deleteConfirmed')
        }
      })
    })
  </script>
@endpush