@section('title', 'Users')
<div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -350px;">
        <div class="row">
          <!-- Sidebar-->
           @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->

          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title-->
                <div class="d-sm-flex align-items-center justify-content-between text-center text-sm-start">
                  <h1 class="h3 mb-2 text-nowrap">Users</h1>
                  <a class="btn btn-translucent-primary">
                    <i class="ai-plus me-2"></i> Add User</a>
                </div>
                <div class="pb-4">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="#">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Users</li>
                    </ol>
                  </nav>
                </div>
                <!-- Follower-->
                @if (!empty($users))
                  @foreach ($users as $user)
                    <div class="d-md-flex align-items-center py-grid-gutter border-bottom">
                      <div class="d-flex align-items-center me-md-4 mb-4 mb-md-0" style="max-width: 264px;"><a class="d-block" href="#"><img class="d-block rounded-circle" src="/frontend/img/dashboard/followers/01.jpg" alt="Emili Parker" width="90"></a>
                        <div class="ps-3">
                          <h2 class="fs-base nav-heading mb-1"><a href="#">{{ $user->name }}</a></h2>
                          <div class="fs-xs text-muted mb-2 pb-1">{{ $user->username }}</div>
                          <button class="btn btn-translucent-primary btn-sm" type="button">Follow</button>
                        </div>
                      </div>
                      <div class="d-flex ms-auto ps-md-1">
                        <a class="d-block" href="#" style="margin-right: 10px;">
                          <img class="d-block" src="img/dashboard/followers/architecture/01.jpg" alt="Architecture" width="102">
                        </a>
                        <a class="d-block" href="#" style="margin-right: 10px;">
                          <img class="d-block" src="img/dashboard/followers/architecture/02.jpg" alt="Architecture" width="102">
                        </a>
                        <a class="d-block" href="#" style="margin-right: 10px;">
                          <img class="d-block" src="img/dashboard/followers/architecture/03.jpg" alt="Architecture" width="102">
                        </a>
                        <a class="d-block" href="#">
                          <img class="d-block" src="img/dashboard/followers/architecture/04.jpg" alt="Architecture" width="102">
                        </a>
                      </div>
                    </div>
                  @endforeach
                @endif
                <!-- Pagination-->
                <nav class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-grid-gutter">
                  <div class="d-md-flex align-items-center w-100"><span class="fs-sm text-muted me-md-3">Showing 6 of 34 followers</span>
                    <div class="progress w-100 my-3 mx-auto mx-md-0" style="max-width: 10rem; height: 4px;">
                      <div class="progress-bar" role="progressbar" style="width: 18%;" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                  <button class="btn btn-outline-primary btn-sm" type="button">Show more followers</button>
                </nav>
              </div>
            </div>
          </div>

          </div>
        </div>  


</div>  


@push('styles')
@endpush

@push('scripts')

@endpush