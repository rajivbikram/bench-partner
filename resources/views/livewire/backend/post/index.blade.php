@section('title', 'Posts')
<div>
	<div class="position-relative bg-gradient" style="height: 480px;">
		<div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
		  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
			<polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
		  </svg>
		</div>
	  </div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -400px;">
        <div class="row">
          <!-- Sidebar-->
           @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->
          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title + Delete link-->
                <div class="d-sm-flex pb-sm-3 align-items-center justify-content-between text-center text-sm-start">
                  <h1 class="h3 mb-2 text-nowrap">Posts</h1>
                  <div class="btn-section">
                    <a class="btn btn-sm btn-translucent-success me-2" href="{{ route('backend.post.import',$username) }}">
                    <i class="ai-download"></i> Import Post
                    </a>
                    <a class="btn btn-sm btn-primary" href="{{ route('backend.post.create',$username) }}">
                      <i class="ai-plus"></i> Add Post
                    </a>
                  </div>
                </div>
                <div class="pb-4">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{ route('home') }}">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Posts</li>
                    </ol>
                  </nav>
                </div>
                @include('layouts.frontend.partials.flash-message')
                <!-- Content-->
                <div class="content">
                	
	               <div class="d-sm-flex justify-content-between mb-3">
		                  <input class="form-control form-control-sm me-3" type="text" wire:model="search" placeholder="Search posts">
		                  <button class="btn btn-sm btn-outline-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Dropdown</button>
		                  <div class="dropdown-menu dropdown-menu-end my-1" style="margin: 0px;"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
	                </div>
					
					<div wire:loading wire:target="search">
						<div class="text-center my-4 justify-content-between">
							<div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
							</div>
							<div class="mt-2">Searching....</div>
						</div>
					</div>
					@if (count($posts)>0)
	                  @forelse ($posts as $post)
					  <div class="card rounded-2 mb-3">
						<div class="card-body py-3">
						  <h5 class="card-title mb-1">
							  {{ $post->post_title }}
						  </h5>
						  <p class="card-text text-muted fs-sm fw-normal">
							<span class="me-1"> 
								<i class="ai-tag"></i> {{ $post->subject->name ?? 'No Subject' }}
							</span>
							<span class="me-1">
								<i class="ai-clock me-1"></i>Aug 04, 2020
							</span>
							<span class="me-1 text-{{ $post->post_status_badge }}">
								<i class="ai-file-text me-1"></i>{{ ucfirst($post->post_status) }}
							</span>
							<span class="me-1">
								<i class="ai-eye me-1"></i>{{ $post->post_view }}
							</span>
							<span class="me-1"><i class="ai-more-vertical"></i></span>
							<span class="me-2">
								<a class="text-primary" href="{{ route('backend.post.edit', [$username, $post]) }}">Edit</a>
							</span>
							<span class="me-2">
								<a class="text-success" href="{{ route('frontend.post.details',$post->post_slug) }}">View</a>
							</span>
							<span class="me-2">
								<a class="text-danger" wire:click.prevent="deleteId({{ $post->id }})" href="#">Delete</a>
							</span>
						  </p>
						</div>
					  </div>
					  @empty
					  <div class="card border-0 mt-2 text-center">
						<div class="card-body">
							<img src="/frontend/img/add-new.svg" alt="Add New">
						  <h4 class="card-title mt-3">No result Found</h4>
						  <p class="card-text">Create new post to hit the button below</p>
							<a class="btn btn-primary" href="{{ route('backend.post.create',$username) }}"><i class="ai-plus me-2"></i> Create post</a>
						</div>
					  </div>
	                  @endforelse
	                  <nav class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-3">
                       <div class="d-md-flex align-items-center">
                          <span class="fs-sm text-muted me-md-3">Showing {{ $posts->firstItem() }} to {{ $posts->lastItem() }}
							of total  entries</span>
                        </div>
                        {{ $posts->links() }}
                      </nav>
                	@else
                    <div class="card border-0 mt-2 text-center">
						<div class="card-body">
							<img src="/frontend/img/add-new.svg" alt="Add New">
							<h4 class="card-title mt-3">No result Found</h4>
							<p class="card-text">Create new post to hit the button below</p>
							<a class="btn btn-primary" href="{{ route('backend.post.create',$username) }}"><i class="ai-plus me-2"></i> Create post</a>
						</div>
						</div>
                  @endif
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>

		@include('layouts.frontend.partials.delete-modal')

</div> 

@push('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script>
     window.addEventListener('delete-conformation', event => {
		Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		if (result.isConfirmed) {
			Livewire.emit('deleteConfirmed')
		}
		})
    })
  </script>
@endpush


