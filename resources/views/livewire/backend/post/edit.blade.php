@section('title', 'Edit Post')
<div>
      <!-- Content-->
        <div class="" id="exampleModalFullscreen" tabindex="-1" aria-labelledby="exampleModalFullscreenLabel" aria-modal="true" role="dialog">
          
          <div class="section-fullscreen bg-white">
              <div class="container">
              <div class="modal-header">
                <h5 class="modal-title h4" id="exampleModalFullscreenLabel">Edit Post</h5>
                <button type="button" class="btn btn-sm btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#cancelEdit" aria-label="Close">
                  <i class="ai-x text-danger"></i>
                </button>
              </div>
              <div class="modal-body">
                @include('layouts.frontend.partials.flash-message')
                <!-- Content-->
                <div class="flat-nav">
                  <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                      <a class="nav-link  p-2 {{  $tabDetails == 'open' ? 'active' : '' }}" id="details-tab" data-bs-toggle="tab" data-bs-target="#details" type="button" role="tab" aria-controls="details" aria-selected="true"><span>Details</span></a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link p-2 {{  $tabSubject == 'open' ? 'active' : '' }}" id="subjects-tab" data-bs-toggle="tab" data-bs-target="#subjects" type="button" role="tab" aria-controls="subjects" aria-selected="false"><span>Subjects</span></a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link p-2 {{  $tabSeo == 'open' ? 'active' : '' }}" id="seo-tab" data-bs-toggle="tab" data-bs-target="#seo" type="button" role="tab" aria-controls="seo" aria-selected="false"><span>SEO</span></a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link p-2 {{  $tabImages == 'open' ? 'active' : '' }}" id="images-tab" data-bs-toggle="tab" data-bs-target="#images" type="button" role="tab" aria-controls="images" aria-selected="false"><span>Images</span></a>
                    </li>
                  </ul>
                </div>
              <form class="needs-validation" wire:ignore wire:submit.prevent="store" novalidate x-data enctype="multipart/form-data">

                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade {{  $tabDetails == 'open' ? 'active show' : '' }}" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-floating mb-4">
                          <input class="form-control form-control-flush" type="text" id="postTitle" wire:model="postTitle" placeholder="Post Title" required>
                          <label for="postTitle">Post Title <span class="required">*</span></label>
                          <div class="invalid-feedback">Post Title is Required.</div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-floating mb-4">
                          <input class="form-control form-control-flush" type="text" id="postSlug" placeholder="Post slug" wire:model.defer="postSlug" required>
                          <label for="postTitle">Slug <span class="required">*</span></label>
                          @if ($postPermalink)
                            <div class="form-text">
                                <strong>Permalink:</strong> <span class=" text-success">{{ $postPermalink }} </span>
                            </div>
                          @endif
                          <div class="invalid-feedback">Post slug is Required.</div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="mb-3 pb-1">
                          <div class="form-floating" wire:ignore>
                            <textarea class="form-control" data-content="@this" id="postContent" wire:model.defer="postContent" style="height: 120px;" placeholder="Post Content">{{ $postContent }}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="mb-3 pb-2">
                          <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input" name="postStatus" wire:model.defer="postStatus" id="status" checked>
                            <label class="form-check-label" for="status">Publish</label>
                          </div>
                        </div>
                        <div class="mb-3 pb-2">
                          <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input" wire:model.defer="isSticky" id="sticky" checked>
                            <label class="form-check-label" for="sticky">Sticky</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-12">
                        <hr class="mt-2 mb-4">
                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                          <div class="form-check d-block">
                            <input class="form-check-input" wire:model.defer="commentStatus"  type="checkbox" id="show-email">
                            <label class="form-check-label" for="show-email">Enable commen box</label>
                          </div>
                          <a class="btn btn-primary btnNext">
                              <i class="ai-arrow-right"></i> Next
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade {{  $tabSubject == 'open' ? 'active show' : '' }}" id="subjects" role="tabpanel" aria-labelledby="subjects-tab">
                    <div class="alert d-flex alert-info" role="alert"><i class="ai-alert-circle fs-xl me-3"></i>
                      <div>Choose the <b>only one</b> subject where you want to publish this post.</div>
                    </div>
                      @if (!empty($subjects))
                        <div class="row mb-1 p-3">
                          @foreach($subjects->chunk(3) as $chunk)
                            <div class="col-sm-4">
                                @foreach ($chunk as $subject)
                                  <div class="form-check mb-2">
                                    <input x-on:click="$wire.getChapters({{ $subject->id }})" wire.model.defer="subjectId" class="form-check-input" name="subjectId" value="{{ $subject->id }}" type="radio" id="{{ $subject->slug }}-{{ $subject->id }}" @if ($subjectId == $subject->id) checked @endif>
                                    <label class="form-check-label text-nav" for="{{ $subject->slug }}-{{ $subject->id }}">
                                      {{ $subject->name }}
                                    </label>
                                  </div>
                                @endforeach
                            </div>
                          @endforeach
                        </div>
                      @endif
                      
                      @if (isset($chapters))
                      <div class="col-sm-12 mb-4 rounded-3 bg-secondary p-4">
                        <div class="widget">
                            <h3 class="widget-title">Chapters</h3>
                          </div>
                          <p class="fs-sm">Select the chapter here.</p>
                          <div class="flex flex-col sm:flex-row items-center">
                              <div class="row">
                                @foreach($chapters->chunk(3) as $chunk)
                                  <div class="col-sm-4">
                                      @foreach ($chunk as $chapter)
                                        <div class="form-check mb-1">
                                          <input class="form-check-input" wire:model="postChapter" name="postChapter" type="radio" value="{{ $chapter->id }}" id="chapter_{{ $chapter->id }}" /> 
                                          <label class="form-check-label text-nav" for="chapter_{{ $chapter->id }}">
                                            {{ $chapter->name }}
                                          </label>
                                        </div>
                                      @endforeach
                                  </div>
                                @endforeach
                              </div>
                          </div>
                      </div>  
                      @endif
                      <div class="col-12">
                        <hr class="mt-2 mb-4">
                        <div class="mb-1">
                          <a class="btn btn-translucent-primary btnPrevious">
                              <i class="ai-arrow-left"></i> Back
                          </a>
                          <div class="mb-3 float-end">
                            <a class="btn btn-primary btnNext">
                                <i class="ai-arrow-right"></i> Next
                            </a>
                         </div>
                        </div>
                      </div>
                  </div>

                  {{-- Seo Tab --}}

                  <div class="tab-pane fade {{  $tabSeo == 'open' ? 'active show' : '' }}" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                      <div class="form-floating mb-4">
                        <textarea class="form-control" wire:model.defer="metaKeyword" style="height: 80px;" rows="1" placeholder="Meta Keyword"></textarea>
                        <label for="fl-textarea">Meta Keyword</label>
                      </div>
                      <div class="form-floating mb-4">
                        <textarea class="form-control" wire:model.defer="metaDescription" style="height: 130px;" rows="4" placeholder="Meta Description"></textarea>
                        <label for="fl-textarea">Meta Description</label>
                      </div>
                      <div class="col-12">
                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                          <div class="widget">
                            <h3 class="widget-title">FAQs for Post</h3>
                          </div>
                        </div>
                      </div>
                      <div class="row mb-4">
                        <div class="col-md-10">
                          <div class="form-group mb-3">
                            <input type="text" class="form-control form-control-sm" placeholder="Question 1" wire:model.defer="faqQuestion.1">
                            @error('faqQuestion.1') <span class="text-danger error">{{ $message }}</span>@enderror
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" wire:model.defer="faqAnswer.1" rows="4" placeholder="Answer 1"></textarea>
                            @error('faqAnswer.1') <span class="text-danger error">{{ $message }}</span>@enderror
                          </div>
                        </div>
                        <div class="col-md-2 d-flex align-items-center">
                          <a class="btn btn-primary btn-sm" wire:click.defer="addFaqRow({{$faqRow}})">
                            <i class="ai-plus fs-lg"></i>Add FAQs
                          </a>
                        </div>
                      </div>
                      @if($editFaqInputs)
                        @foreach($editFaqInputs as $key=>$faq)
                          <div class="row mb-4">
                              <div class="col-md-10">
                                <div class="form-group mb-3">
                                  <input type="text" class="form-control" value="{{ $faq['question'] }}">
                                  @error('faqQuestion.{{$loop->iteration}}') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                  <textarea class="form-control" rows="4">{{  $faq['answer']  }}</textarea>
                                  @error('faqAnswer.{{$loop->iteration}}') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                              </div>
                              <div class="col-md-2 d-flex align-items-center">
                                <button class="btn btn-danger btn-sm" wire:click.prevent="removeFaqRow({{$loop->iteration}})">
                                  <i class="ai-trash fs-lg"></i> Remove
                                </button>
                              </div>
                          </div>
                        @endforeach
                      @else
                        @foreach($faqInputs as $key => $value)
                          <div class="row mb-4">
                              <div class="col-md-10">
                                <div class="form-group mb-3">
                                  <input type="text" class="form-control form-control-sm" placeholder="Question {{ $value }}" wire:model.defer="faqQuestion.{{ $value }}">
                                  @error('faqQuestion.{{ $value }}') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                  <textarea class="form-control" wire:model.defer="faqAnswer.{{ $value }}" rows="4" placeholder="Answer {{ $value }}"></textarea>
                                  @error('faqAnswer.{{ $value }}') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                              </div>
                              <div class="col-md-2 d-flex align-items-center">
                                <button class="btn btn-danger btn-sm" wire:click.prevent="removeFaqRow({{$key}})">
                                  <i class="ai-trash fs-lg"></i> Remove
                                </button>
                              </div>
                          </div>
                        @endforeach
                      @endif
                      
                      <div class="col-12">
                        <hr class="mt-2 mb-4">
                        <div class="mb-1">
                          <a class="btn btn-translucent-primary btnPrevious">
                              <i class="ai-arrow-left"></i> Back
                          </a>
                          <div class="mb-3 float-end">
                            <a class="btn btn-primary btnNext">
                                <i class="ai-arrow-right"></i> Next
                            </a>
                         </div>
                        </div>
                      </div>
                  </div>

                   {{-- Images Tab --}}

                  <div class="tab-pane fade {{  $tabImages == 'open' ? 'active show' : '' }}" id="images" role="tabpanel" aria-labelledby="images-tab">
                    <div class="col-sm-12">
                        <div class="mb-3 pb-1">
                          <div class="widget">
                            <h3 class="widget-title">Feature Image</h3>
                              <div class="file-drop-area">
                                <div class="file-drop-icon ai-upload"></div>
                                <span class="file-drop-message">Drag and drop here to upload</span>
                                <input type="file" id="featuredImage" wire:model.defer="featuredImage" class="file-drop-input"  accept="image/*">
                                <button type="button" class="file-drop-btn btn btn-translucent-primary btn-sm">Or select file</button>
                              </div>
                              
                              <div class="form-check mt-4 d-block">
                                <input class="form-check-input" type="checkbox" wire:model.defer="hideFeaturedImage" id="hideImage">
                                <label class="form-check-label" for="hideImage">Hide feature image</label>
                              </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12">
                        <hr class="mt-2 mb-4">
                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                          <a class="btn btn-translucent-primary btnPrevious">
                              <i class="ai-arrow-left"></i> Back
                          </a>
                          <button id="submit" class="btn btn-primary mt-3 mt-sm-0" type="submit">
                            <span wire:loading.remove wire:target="store"><i class="ai-save fs-lg me-2"></i> Save Changes</span>
                            <span wire:loading wire:target="store"> 
                              <span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span> 
                              Saving...
                            </span>
                          </button>
                        </div>
                      </div>
                  </div>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div wire:ignore.self class="modal fade" id="cancelEdit" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title fw-bold">You have unsaved changes</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <p>You've made changes to your post. Do you want to navigate to leave this page?</p>
              <div class="pb-2">
                <a href="{{ route('backend.post.index',$username) }}" class="btn btn-danger btn-shadow btn-sm">
                   Yes, leave the page
                </a>
                <button class="btn btn-primary btn-sm" type="button" data-bs-dismiss="modal">No, keep editing</button>
                  
              </div>
          </div>
        </div>
      </div>
    </div>

</div>

@push('styles')
  
<script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/ckeditor.js"></script>
<script src="/frontend/vendor/ckeditor5/src/ckeditor.js"></script>

@endpush

@push('scripts')
<script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>


<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="/frontend/vendor/trumbowyg/jquery-3.3.1.min.js"></script>



<script>
  $('.btnNext').click(function () {
      $('.nav-tabs > .nav-item > .active').parent().next('li').find('.nav-link span').trigger('click');
  });

  $('.btnPrevious').click(function () {
      $('.nav-tabs > .nav-item > .active').parent().prev('li').find('.nav-link span').trigger('click');
  });
  
  ClassicEditor
    .create( document.querySelector( '#postContent' ) )
    .then(editor =>{
        document.querySelector('#submit').addEventListener('click', () =>{
            let content = $('#postContent').data('content');
            eval(content).set('postContent', editor.getData());
        });
    })
    .catch( error => {
        console.error( error );
    } );

</script>
@endpush


@push('seo')

  
  <meta name="description" content="Explain the Features, Principles and Layers of OSI/ISO Reference Model with Neat Diagram. Layers of OSI model Diagram in data communications, osi referance model"/>
  <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large"/>
  <link rel="canonical" href="https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/" />
  <meta property="og:locale" content="en_US">
  <meta property="og:type" content="article">
  <meta property="og:title" content="Explain the Layers of OSI/ISO Reference Model with Neat Diagram">
  <meta property="og:description" content="Explain the Features, Principles and Layers of OSI/ISO Reference Model with Neat Diagram. Layers of OSI model Diagram in data communications, osi referance model">
  <meta property="og:url" content="https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/">
  <meta property="og:site_name" content="Bench Partner">
  <meta property="article:section" content="Data Communications">
  <meta property="og:updated_time" content="2020-10-19T18:11:21+00:00">
  <meta property="og:image" content="https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png">
  <meta property="og:image:secure_url" content="https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png">
  <meta property="og:image:width" content="466">
  <meta property="og:image:height" content="526">
  <meta property="og:image:alt" content="Explain the Layers of OSI model with Net Diagram">
  <meta property="og:image:type" content="image/png">
  <meta property="article:published_time" content="2019-07-06GMT+000017:46:18+00:00">
  <meta property="article:modified_time" content="2020-10-19GMT+000018:11:21+00:00">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Explain the Layers of OSI/ISO Reference Model with Neat Diagram">
  <meta name="twitter:description" content="Explain the Features, Principles and Layers of OSI/ISO Reference Model with Neat Diagram. Layers of OSI model Diagram in data communications, osi referance model">
  <meta name="twitter:image" content="https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png">
  <script type="application/ld+json" class="rank-math-schema">
  {"@context":"https://schema.org",
  "@graph":[{
  "@type":["Person","Organization"],
  "@id":"https://benchpartner.com/#person",
  "name":"Bench Partner",
  "logo":{"@type":"ImageObject","url":"https://benchpartner.com/wp-content/uploads/2020/07/logo.png"},
  "image":{"@type":"ImageObject","url":"https://benchpartner.com/wp-content/uploads/2020/07/logo.png"}},
  {"@type":
  "WebSite","@id":"https://benchpartner.com/#website","url":"https://benchpartner.com","name":"Bench Partner",
  "publisher":{"@id":"https://benchpartner.com/#person"},"inLanguage":"en-US"},
  {"@type":"ImageObject","@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#primaryImage","url":"https://benchpartner.com/wp-content/uploads/2019/07/Explain-the-Layers-of-OSI-model-with-Net-Diagram.png","width":"466","height":"526"},

  {"@type":"WebPage","@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#webpage","url":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/","name":"Explain the Layers of OSI/ISO Reference Model with Neat Diagram","datePublished":"2019-07-06T17:46:18+00:00","dateModified":"2020-10-19T18:11:21+00:00","isPartOf":{"@id":"https://benchpartner.com/#website"},"primaryImageOfPage":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#primaryImage"},"inLanguage":"en-US"},

  {"@type":"BlogPosting","headline":"Explain the Layers of OSI/ISO Reference Model with Neat Diagram","datePublished":"2019-07-06GMT+000017:46:18+00:00","dateModified":"2020-10-19GMT+000018:11:21+00:00","author":

  {"@type":"Person","name":"Bench Partner"},"description":"Explain the Features, Principles and Layers of OSI/ISO Reference Model with Neat Diagram. Layers of OSI model Diagram in data communications, osi referance model","@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#schema-10821","isPartOf":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#webpage"},

  "publisher":{"@id":"https://benchpartner.com/#person"},"image":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#primaryImage"},"inLanguage":"en-US","mainEntityOfPage":{"@id":"https://benchpartner.com/explain-the-osi-iso-reference-model-with-neat-diagram/#webpage"}}]}

  </script>
  
@endpush