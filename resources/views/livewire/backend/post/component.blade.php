<!DOCTYPE html>
<html lang="en">
  

@include('layouts.frontend.head')  


<!-- Body-->
  <body>
    <div class="main">

      <div class="main-content">

        @yield('content')

      </div>

      <!-- Back to top button-->
      <a class="btn-scroll-top" href="#top" data-scroll data-fixed-element><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ai-arrow-up">   </i></a>

    </div>

  </body>

  @livewireScripts
   
<!-- Vendor JS -->
<script src="/backend/js/vendor.bundle.js"></script>

<!-- Theme JS -->
<script src="/backend/js/theme.bundle.js"></script>

  @stack('scripts')


</html>