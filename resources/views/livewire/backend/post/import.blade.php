@section('title', 'Import Posts')
<div>
  <div class="position-relative bg-gradient" style="height: 480px;">
		<div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
		  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
			<polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
		  </svg>
		</div>
	  </div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -400px;">
        <div class="row">
          <!-- Sidebar-->
           @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->
          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title + Delete link-->
                <div class="d-sm-flex align-items-center justify-content-between text-center text-sm-start">
                  <h1 class="h3 mb-2 text-nowrap">Import Post</h1>
                  <a class="btn btn-translucent-primary" href="{{ route('backend.post.create',$username) }}">
                    <i class="ai-plus me-2"></i> Add Post</a>
                </div>
                <div class="pb-2">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{ route('home') }}">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item">
                        <a href="{{ route('backend.post.index',$username) }}">
                          Posts
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Import Post</li>
                    </ol>
                  </nav>
                </div>
                
                <!-- Content-->
                <div class="content">
                    <div class="card border-0 mt-4 text-center">
                      <div class="card-body">
                        @include('layouts.frontend.partials.flash-message')
                        <form wire:submit.prevent="import" enctype="multipart/form-data">
                          <div class="row">
                            <div class="col-sm-12" wire:ignore.self>
                              <div class="form-floating mb-3">
                                <div class="file-drop-area">
                                  <div class="file-drop-icon ai-upload"></div>
                                  <span class="file-drop-message">Drag and drop CSV / Excel file here to upload</span>
                                  <input type="file" wire:model.defer="file" class="file-drop-input">
                                  <button type="button" class="file-drop-btn btn btn-translucent-primary btn-sm">Or select file</button>
                                </div>
                              </div>
                            </div>
                            <div wire:loading wire:target="import">
                              <div class="col-12">
                                <h4 class="text-primary">Importing Post ....</h4>
                                <div class="progress mb-3">
                                  <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                              </div>
                            </div>
                            
                            <div class="col-12">
                              <div class="mt-3">
                                <button class="btn btn-primary d-block w-100 " type="submit">
                                    <i class="ai-upload"></i> Import Post
                                </button>
                              </div>
                            </div>
                          </div>
                        </form>
                        <div class="col-12">
                          <div class="mt-4">
                            <a class="btn btn-link" wire:model="example">
                                <i class="ai-download"></i> Download Example File
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>

</div>  


