@section('title', 'Roles')
<div>
  <div class="position-relative bg-gradient" style="height: 480px;">
		<div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
		  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
        <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
        </svg>
      </div>
	  </div>
  <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -400px;">
        <div class="row">
          <!-- Sidebar-->
           @include('layouts.frontend.dashboard.sidebar')
          <!-- Content-->
          <div class="col-lg-9">
            <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
              <div class="py-2 p-md-3">
                <!-- Title + Delete link-->
                <div class="d-sm-flex align-items-center justify-content-between text-center text-sm-start">
                  <h1 class="h3 mb-2 text-nowrap">Roles</h1>
                  <a class="btn btn-translucent-primary mb-2" wire:click="create" data-bs-toggle="modal"  data-bs-target="#addModal">
                    <i class="ai-plus me-2"></i> Add Role</a>
                </div>
                <div class="pb-4">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="#">
                          <i class="ai-home"></i>
                          Home
                        </a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Roles</li>
                    </ol>
                  </nav>
                </div>
                @include('layouts.frontend.partials.flash-message')
                <!-- Content-->
                <div class="content">
                  @if (count($roles)>0)
                    <div class="d-sm-flex justify-content-between mb-3">
                        <input class="form-control" type="text" wire:model="search" placeholder="Search Roles">
                    </div>
                    <div class="table-responsive fs-md">
                          @foreach ($roles as $role)
                            <div class="accordion" id="orders-accordion">
                              <div class="accordion-item">
                                <h2 class="accordion-header" id="role-header-{{ $role->id }}">
                                  <div class="accordion-button collapsed accordion-button no-indicator d-flex flex-wrap align-items-center justify-content-between pe-4" type="button" data-bs-toggle="collapse" data-bs-target="#role-collapse-{{ $role->id }}" aria-expanded="false" aria-controls="role-collapse-{{ $role->id }}">
                                    <div class="fs-sm text-nowrap my-1 me-2"><i class="ai-hash fs-base me-1"></i><span class="d-inline-block align-middle">{{ $role->name }}</span></div>
                                    <div class="text-nowrap text-body fw-medium fs-sm my-1 me-2"></i>{{ $role->slug }}</div>
                                    <div class="fs-xs fw-medium py-1 px-3 my-1 me-2">{{ $role->description }}</div>
                                    <div class="text-body fs-sm my-1">
                                      <a class="nav-link-style me-2 text-primary" wire:click="edit({{ $role->id }})" href="#" data-bs-toggle="modal"  data-bs-target="#addModal" data-bs-toggle="tooltip" data-bs-original-title="Edit" aria-label="Edit"><i class="ai-edit"></i></a>
                                      <a class="nav-link-style text-danger" wire:click.prevent="deleteId({{ $role->id }})" href="#" title="Remove">
                                      <i class="ai-trash-2"></i></a>
                                    </div>
                                  </div>
                                </h2>
                              </div>
                            </div>
                          @endforeach
                          <nav class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-3">
                            {{ $roles->links() }}
                          </nav>
                    </div>
                  @else
                    <div class="card border-0 mt-2 text-center">
                      <div class="card-body">
                        <img src="/frontend/img/add-new.svg" alt="Add New">
                        <h4 class="card-title mt-3">Create new Role</h4>
                        <p class="card-text">Create new role to hit the button below</p>
                          <a class="btn btn-primary" wire:click="create" data-bs-toggle="modal"  data-bs-target="#addModal" href="#"><i class="ai-plus me-2"></i> Create Role</a>
                      </div>
                    </div>
                  @endif
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>


    {{-- Add Model --}}
    <div wire:ignore.self class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content border-0">
            <div class="view show" id="modal-signup-view">
              <div class="modal-header px-4">
                <h4 class="modal-title">Add Role</h4>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="btn-close"></button>
              </div>
              <div class="modal-body px-4">
                <form wire:submit.prevent="store">
                  <div class="mb-3">
                      <input type="text" wire:model="name" 
                           id="roleName" 
                           class="form-control @error('name') is-invalid @enderror" 
                           placeholder="Name"
                           autofocus>

                      <input type="hidden" 
                           wire:model="slug" 
                           id="roleSlug">

                          @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror

                      <div id="roleSlugHelp" class="form-text" wire:model="slug"></div>
                    </div>
                    <div class="mb-3">
                      <textarea class="form-control" wire:model="description" placeholder="Description"></textarea>
                    </div>

                    <button class="btn btn-danger" data-bs-dismiss="modal" aria-label="btn-close">
                            Cancel
                    </button>
                    <button class="btn btn-primary" type="submit" wire:click="$emit('roleCreated')">
                      <i class="ai-check"></i> Save
                    </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
     


</div>  


@push('styles')

@endpush


@push('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script>
     window.addEventListener('delete-conformation', event => {
      Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
      if (result.isConfirmed) {
        Livewire.emit('deleteConfirmed')
      }
      })
    })
  </script>
@endpush