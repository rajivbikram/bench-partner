<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {

    }
    
    public function create()
    {
        return view('backend.post.create');
    }

    public function edit($username, $id)
    {
        $post = Post::findOrFail($id);
        return view('backend.post.edit',compact('post'));
    }
}
