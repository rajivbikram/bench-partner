<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        return view('backend.post.index');
    }
    
    public function create()
    {
        return view('backend.post.create');
    }

    public function edit($username, $id)
    {
        $subject = Subject::findOrFail($id);
        return view('backend.subject.edit',compact('subject'));
    }
}
