<?php

namespace App\Http\Livewire\Backend\Chapter;

use App\Models\Subject;
use App\Models\Chapter;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class ChapterComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $name, $slug, $subjectId, $deleteId, $order, $chapterId;
    public $search = '';

    protected $listeners = ['deleteConfirmed' => 'delete'];

    public function render()
    {
        $subjects = Subject::active()->get();
        $chapters = Chapter::search($this->search)
                ->latest()
                ->paginate(10);
                
        return view('livewire.backend.chapter.index',compact('chapters','subjects'));
    }

    public function create()
    {
        $this->resetInputFields();
    }

    private function resetInputFields()
    {
        $this->name = '';
        $this->slug = '';
        $this->subjectId = '';
        $this->order = '';
    }

    public function updatedName()
    {
        $this->slug = SlugService::createSlug(Chapter::class, 'slug', $this->name);
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|string',
        ]);
        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'subject_id' => $this->subjectId,
            'order' => $this->order,
            'created_by' => Auth::user()->id,
        );
        $chapter = Chapter::updateOrCreate(['id' => $this->chapterId],$data);
        session()->flash('success', $this->chapterId ? 'Chapter Updated Successfully. 😁' : 'Chapter Created Successfully. 😁');
        $this->resetInputFields();
        $this->dispatchBrowserEvent('closeModel');
    }

    public function edit($id)
    {
        $chapter = Chapter::findOrFail($id);
        $this->chapterId = $id;
        $this->name = $chapter->name;
        $this->slug = $chapter->slug;
        $this->order = $chapter->order;
        $this->subjectId = $chapter->subject_id;
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
        $this->dispatchBrowserEvent('delete-conformation');
    }

    public function delete()
    {
        Chapter::find($this->deleteId)->delete();
        session()->flash('success', 'Chapter Deleted Successfully.');
        $this->emit('hideDeleteModel');
    }

}
