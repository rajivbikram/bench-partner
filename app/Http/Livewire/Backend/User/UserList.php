<?php

namespace App\Http\Livewire\Backend\User;

use App\Models\User;
use Livewire\Component;

class UserList extends Component
{
	public $users;

	public function mount()
	{
		$this->users = User::all();
	}

    public function render()
    {
        return view('livewire.backend.user.index',['users'])
                ->extends('layouts.frontend.dashboard.app')
                ->section('content');
    }
}