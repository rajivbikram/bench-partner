<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class DashboardComponent extends Component
{

    public function render()
    {
        return view('livewire.backend.dashboard.index');
    }
}
