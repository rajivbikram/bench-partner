<?php

namespace App\Http\Livewire\Backend\Posts;

use App\Models\Subject;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use App\Imports\PostImport;
use App\Traits\ImportFile;
use Excel;

class ImportPost extends Component
{
	use WithFileUploads, ImportFile;
	
	public $subjects, $file;

	public function mount()
	{
		$this->subjects = Subject::active()->get();
	}

    public function render()
    {
    	return view('livewire.backend.post.import',['subjects'])
                ->extends('layouts.frontend.dashboard.app')
                ->section('content');
    }

	public function example()
	{
		$path = '/public/bp-content/example/post-import-example.csv';
		return response()->download($path, $fileName, [
			'Content-Type' => 'application/vnd.ms-excel',
			'Content-DDisposition' => 'inline; filename="'. $fileName .'"'
		]);
	}

	public function import()
	{
		$this->validate([
            'file' => 'required',
        ]);

		if (!empty($this->file)) {

			$uploadedFile = $this->uploadOne($this->file);
			Excel::import(new PostImport, $uploadedFile);
			$this->deleteOne($uploadedFile);
			session()->flash('success', 'Post Imported Successfully. 😁');
        }
	}
}