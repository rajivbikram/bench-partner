<?php

namespace App\Http\Livewire\Backend\Posts;

use App\Models\Subject;
use App\Models\Chapter;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;
use Livewire\Component;

class EditPost extends Component
{

	use WithFileUploads;

    public $subjects, $chapters, $postTitle, $postSlug, $subjectId, $postChapter, $postContent, $featuredImage, $hideFeaturedImage, $isSticky, $postStatus, $postType, $postPermalink, $postLike, $postShortTitle, $commentStatus, $createdBy, $updatedBy;

    public $metaKeyword, $metaDescription, $seoImage, $postFaq;
    public $faqRow = 1, $faqInputs = [], $editFaqInputs = [], $faqQuestion, $faqAnswer;
    public $tabDetails = null, $tabSeo = null, $tabImages = null, $tabSubject = null;
    public $postId, $post;
    public $state = [];
   
    public function getChapters($id)
    {
        
        $chapters = Chapter::where('subject_id', $id)->get();
        $this->subjectId = $id;
        $this->chapters = $chapters;
        $this->tabDetails = 'close';
        $this->tabSubject = 'open';
        $this->tabSeo = 'close';
    }

    public function mount($post)
    {
        $this->subjects = Subject::active()->get();
        if ($post) {
            $this->editPost($post);
        }
        $this->tabDetails = 'open';
    }

    public function updatedPostTitle()
    {
        $this->postSlug = SlugService::createSlug(Post::class, 'post_slug', $this->postTitle);
        $this->postPermalink = request()->getHttpHost() .'/'. SlugService::createSlug(Post::class, 'post_slug', $this->postTitle);
        $this->tabDetails = 'open';
        $this->tabSubject = 'close';
    }

    public function render()
    {
        $post = $this->post;
        return view('livewire.backend.post.edit')
                ->extends('livewire.backend.post.component');
    }

    private function editPost($id)
    {
        $post = Post::findOrFail($id);
        $this->postId = $post->id;
        $this->postTitle = $post->post_title;
        $this->postSlug = $post->post_slug;
        $this->postContent = $post->post_content;
        $this->subjectId = $post->subject_id;
        $this->chapters = Chapter::where('subject_id', $this->subjectId)->get();

        $this->postChapter = $post->chapter_id;
        $this->featuredImage = $post->featured_image;
        $this->hideFeaturedImage = $post->hide_featured_image;

        $this->isSticky = $post->is_sticky;
        $this->postStatus = $post->post_status;
        $this->postType = $post->post_type;
        $this->postPermalink = $post->post_permalink;

        $this->postShortTitle = $post->post_short_title;
        $this->commentStatus = $post->comment_status;
        $this->createdBy = $post->created_by;
        $this->updatedBy = $post->updated_by;

        $this->editFaqInputs = unserialize($post->post_faq);
        $this->metaKeyword = $post->meta_keyword;
        $this->metaDescription = $post->meta_description;
        $this->seoImage = $post->seo_image;
    }

    public function store()
    {
        $this->validate([
            'postTitle' => 'required|string',
        ]);

        $data = array(
            'post_title' => $this->postTitle,
            'post_slug' => $this->postSlug,
            'subject_id' => $this->subjectId,
            'chapter_id' => $this->postChapter,
            'post_content' => $this->postContent,
            'hide_featured_image' => $this->hideFeaturedImage ? 1 : 0,
            'is_sticky' => $this->isSticky ? 1 : 0,
            'post_status' => $this->postStatus ? 'publish' : 'draft',
            'post_type' => 'post',
            'post_permalink' => $this->postPermalink,
            'post_short_title' => $this->postShortTitle,
            'comment_status' => $this->commentStatus ? 'open' : 'close',
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
            'meta_keyword' => $this->metaKeyword,
            'meta_description' => $this->metaDescription
        );
        
        $post = Post::updateOrCreate(['id' => $this->postId],$data);
        session()->flash('success', $this->postId ? 'Post Updated Successfully. 😁' : 'Post Created Successfully. 😁');
        $this->tabDetails = 'open';
        $this->tabSubject = 'close';
    }

    public function uploadFile()
    {
        if (!$this->featuredImage) {
            return null;
        }

        // $img = ImageManagerStatic::make($this->featuredImage)->encode('jpg');
        // $name = Str::random(). '.jpg';
        // Storage::disk('public')->put($name, $img);
        // return $name;

        // $image = Image::make($featuredImage->getPathName());
        // $extension = $featuredImage->getClientOriginalExtension();
        // $imageName = Str::slug($this->featuredImage). '.' . strtolower($extension);
        // $imagePath = date('Y/m') . '/' . $imageName;
        // $image->resize(1920, 1080)->save($imagePath);
        // return $imagePath;
    }

    public function addFaqRow($faqRow)
    {
        $faqRow = $faqRow + 1;
        $this->faqRow = $faqRow;
        array_push($this->editFaqInputs ,$faqRow);
        $this->tabSeo = 'open';
        $this->tabDetails = 'close';
        $this->tabSubject = 'close';
        $this->tabImages = 'close';
    }

    public function removeFaqRow($faqRow)
    {
        unset($this->editFaqInputs[$faqRow]);
        $this->tabSeo = 'open';
        $this->tabDetails = 'close';
        $this->tabSubject = 'close';
        $this->tabImages = 'close';
    }
}