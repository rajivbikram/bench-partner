<?php

namespace App\Http\Livewire\Backend\Posts;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class ListPost extends Component
{
	use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '', $deleteId;

    protected $listeners = ['deleteConfirmed' => 'delete'];

    public function render()
    {
    	$posts = Post::search($this->search)
                ->autherPost()
                ->with('subject','chapter')
    			->latest()
                ->paginate(10);

        return view('livewire.backend.post.index',compact('posts'));
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
        $this->dispatchBrowserEvent('delete-conformation');
    }

    public function delete()
    {
        try {
        	$post = Post::find($this->deleteId);
            if (isset($post)) {
		        $post->delete();
		        session()->flash('success', 'Post Deleted Successfully.');
            }else{
                session()->flash('error', 'Id could not be obtained!');
            }
            
         } catch (\Exception $e) {
            return $e->getMessage();
            session()->flash('error', 'Exception : ' . $e);
        }
    }
}
