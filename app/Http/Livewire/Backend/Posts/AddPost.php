<?php

namespace App\Http\Livewire\Backend\Posts;

use App\Models\Subject;
use App\Models\Chapter;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;
use Livewire\Component;

class AddPost extends Component
{
    use WithFileUploads;

	public $postId, $subjects, $chapters, $postTitle, $postSlug, $subjectId, $postChapter, $postContent, $featuredImage, $hideFeaturedImage, $isSticky, $postStatus, $postType, $postPermalink, $postLike, $postShortTitle, $commentStatus, $createdBy, $updatedBy;
    public $metaKeyword, $metaDescription, $seoImage, $postFaq;
    public $faqRow = 1, $faqInputs = [], $faqQuestion, $faqAnswer;
    public $tabDetails = null, $tabSeo = null, $tabImages = null, $tabSubject = null;
    
    public function getChapters($id)
    {
        $chapters = Chapter::where('subject_id', $id)->get();
        $this->subjectId = $id;
        $this->chapters = $chapters;
        $this->tabDetails = 'close';
        $this->tabSubject = 'open';
        $this->tabSeo = 'close';
    }

	public function mount()
	{
		$this->subjects = Subject::active()->get();
        $this->tabDetails = 'open';
	}

    public function updatedPostTitle()
    {
        $this->postSlug = SlugService::createSlug(Post::class, 'post_slug', $this->postTitle);
        $this->postPermalink = request()->getHttpHost() .'/'. SlugService::createSlug(Post::class, 'post_slug', $this->postTitle);
        $this->tabDetails = 'open';
        $this->tabSubject = 'close';
    }

    public function render()
    {
    	return view('livewire.backend.post.create',['categories','chapters'])
                ->extends('livewire.backend.post.component');
    }

    public function store()
    {
        $this->validate([
            'postTitle' => 'required|string',
            'postSlug' => 'required|string',
            'subjectId' => 'required',
        ]);
        $userName = Auth::user()->username;
        $post = new Post;
        $post->post_title = $this->postTitle;
        $post->post_slug = $this->postSlug;
        $post->subject_id = $this->subjectId;
        $post->chapter_id = $this->postChapter;
        $post->post_content = $this->postContent;
        $post->hide_featured_image = $this->hideFeaturedImage ? 1 : 0;
        $post->is_sticky = $this->isSticky ? 1 : 0;
        $post->post_status = $this->postStatus ? 'publish' : 'draft';
        $post->post_type = 'post';
        $post->post_permalink = $this->postPermalink;
        $post->post_short_title = $this->postShortTitle;
        $post->comment_status = $this->commentStatus ? 'open' : 'close';
        $post->created_by = auth()->id();
        $post->updated_by = auth()->id();
        $post->meta_keyword = $this->metaKeyword;
        $post->meta_description = $this->metaDescription;
        
        if($this->faqQuestion && $this->faqAnswer){
            foreach($this->faqQuestion as $key=>$question){
                $faqInputs[$key]['question'] = $question;
            }
            foreach($this->faqAnswer as $key=>$answer){
                $faqInputs[$key]['answer'] = $answer;
            }
            $post->post_faq = serialize($faqInputs);
        }
        $post->save();
        session()->flash('success', 'Post Created Successfully. 😁');
        return redirect()->route('backend.post.edit',[$userName, $post->id]);
    }

    public function addFaqRow($faqRow)
    {
        $faqRow = $faqRow + 1;
        $this->faqRow = $faqRow;
        array_push($this->faqInputs ,$faqRow);
        $this->tabSeo = 'open';
        $this->tabDetails = 'close';
        $this->tabSubject = 'close';
    }

    public function removeFaqRow($faqRow)
    {
        unset($this->faqInputs[$faqRow]);
        $this->tabSeo = 'open';
        $this->tabDetails = 'close';
        $this->tabSubject = 'close';
    }

    


}
