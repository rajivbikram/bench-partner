<?php

namespace App\Http\Livewire\Backend\Setting;

use Livewire\Component;

class ProfileComponent extends Component
{
    public function render()
    {
        return view('livewire.backend.setting.profile');
    }
}
