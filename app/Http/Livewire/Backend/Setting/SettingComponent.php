<?php

namespace App\Http\Livewire\Backend\Setting;

use Livewire\Component;

class SettingComponent extends Component
{
    public function render()
    {
        return view('livewire.backend.setting.component');
    }
}
