<?php

namespace App\Http\Livewire\Backend\Category;

use Livewire\Component;

class Edit extends Component
{
    public function render()
    {
        return view('livewire.backend.category.edit');
    }
}
