<?php

namespace App\Http\Livewire\Backend\Category;

use Livewire\Component;

class Add extends Component
{
    public function render()
    {
        return view('livewire.backend.category.add');
    }
}
