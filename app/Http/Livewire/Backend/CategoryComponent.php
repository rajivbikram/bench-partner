<?php

namespace App\Http\Livewire\Backend;

use App\Models\Category;
use App\Models\SeoData;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class CategoryComponent extends Component
{
	use WithPagination;

	protected $paginationTheme = 'bootstrap';
	public $name, $slug, $description, $categoryId, $metaKeyword, $metaDescription, $status, $image, $icon;
    public $search = '';

    protected $listeners = ['fileUpload' => 'handelFileUpload'];

    public function handelFileUpload($imageData)
    {
    	$this->image = $imageData;
    }

    public function render()
    {
        $categories = Category::search($this->search)
        		->with('seo')
        		->latest()
        		->paginate(10);

        return view('livewire.frontend.category.category-component',compact('categories'))
                ->extends('layouts.app')
                ->section('content');
    }

    public function create()
    {
        $this->resetInputFields();
    }

    private function resetInputFields()
    {
        $this->name = '';
        $this->description = '';
        $this->status = '';
        $this->slug = '';
        $this->menu = '';
        $this->image = '';
        $this->view = '';
        $this->icon = '';
        $this->created_by = '';
        $this->updated_by = '';
        $this->metaKeyword = '';
        $this->metaDescription = '';
    }

    public function updatedName()
    {
        $this->slug = SlugService::createSlug(Category::class, 'slug', $this->name);
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|string',
        ]);

        $image = $this->storeImage();

        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'status' => $this->status ? 'active' : 'inactive',
            'icon' => $this->icon,
            'image' => $image
        );
        

        $category = Category::updateOrCreate(['id' => $this->categoryId],$data);

        if(isset($this->metaDescription) && ($this->metaKeyword)){
            $seoData = new SeoData;
            $seoData->seo_id = $category->id;
            $seoData->type = 'category_seo';
            $category_seo=[];
            $category_seo['meta_description'] = $this->metaDescription;
            $category_seo['meta_keyword'] = $this->metaKeyword;
            $seoData->value = serialize($category_seo);
            if (isset($this->categoryId)) {
            	SeoData::where('seo_id', $category->id)->where('type', 'category_seo')->delete();
            }
            $seoData->save();
        }

        session()->flash('success', $this->categoryId ? 'Category Updated Successfully. 😁' : 'Category Created Successfully. 😁');
        $this->resetInputFields();
        $this->emit('closeModel');
    }

    public function storeImage()
    {
    	if (!$this->image) {
    		return null;
    	}

    	$img = ImageManagerStatic::make($this->image)->encode('jpg');
    	$name = Str::random(). '.jpg';
    	Storage::disk('public')->put($name, $img);
    	return $name;
    }

    public function edit($id)
    {
        $category = Category::with('seo')->findOrFail($id);
        $this->categoryId = $id;
        $this->name = $category->name;
        $this->slug = $category->slug;
        $this->status = $category->status ? 'active' : 'inactive';
        $this->description = $category->description;
        $this->icon = $category->icon;
        $this->image = $category->image_path;

        $seoData=[];
       	if (!empty($category->seo)){
       		if($category->seo->type == 'category_seo'){
	            $seoData = unserialize($category->seo->value);
		        $this->metaKeyword = $seoData['meta_keyword']; 
		        $this->metaDescription = $seoData['meta_description'];
		    }
    	}
    }

    public function delete($id)
    {
        try {
            $this->categoryId = $id;
        	$category = Category::with('seo')->find($id);
            if (isset($category)) {
                SeoData::where('seo_id', $category->id)->where('type', 'category_seo')->delete();
		        $category->delete();
		        session()->flash('success', 'Category Deleted Successfully.');
            }else{
                session()->flash('error', 'Id could not be obtained!');
            }
            
         } catch (\Exception $e) {
            return $e->getMessage();
            session()->flash('error', 'Exception : ' . $e);
        }
    }
}
