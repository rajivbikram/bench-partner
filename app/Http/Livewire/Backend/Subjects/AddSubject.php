<?php

namespace App\Http\Livewire\Backend\Subjects;

use App\Models\Subject;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Traits\UploadImage;

class AddSubject extends Component
{
	use WithFileUploads, UploadImage;

	protected $paginationTheme = 'bootstrap';
	public $name, $slug, $description, $subjectId, $hideImage, $menu, $status, $image, $icon, $logo, $color, $order, $view;
    public $metaKeyword, $metaDescription, $seoImage;
    public $search = '';

    public function render()
    {
        $subjects = Subject::search($this->search)
        		->with('seo')
        		->latest()
        		->paginate(10);

        return view('livewire.backend.subjects.add-subject');
    }

    public function updatedName()
    {
        $this->slug = SlugService::createSlug(Subject::class, 'slug', $this->name);
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|string',
            'slug' => 'required|unique:subjects|string',
            'image' => 'image|max:10024',
            'seoImage' => 'image|max:10024',
        ]);
        if ($this->image) {
    		$image = $this->uploadImage($this->image);
    	}
        if ($this->seoImage) {
    		$seoImage = $this->uploadImage($this->seoImage);
    	}
        
        $userName = Auth::user()->username;  
        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'menu' => $this->menu,
            'status' => $this->status ? 'active' : 'inactive',
            'icon' => $this->icon,
            'view' => $this->view,
            'color' => $this->color,
            'order' => $this->order,
            'meta_keyword' => $this->metaKeyword,
            'meta_description' => $this->metaDescription,
            'image' => $image ?? null,
            'seo_image' => $seoImage ?? null,
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
        );
        
        $subject = Subject::create($data);
        session()->flash('success', 'Subject Created Successfully. 😁');
        return redirect()->route('backend.subject.edit',[$userName, $subject->id]);
    }
}

