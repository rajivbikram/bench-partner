<?php

namespace App\Http\Livewire\Backend\Subjects;

use App\Models\Subject;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Traits\UploadImage;

class EditSubject extends Component
{
	use WithFileUploads, UploadImage;

	protected $paginationTheme = 'bootstrap';
	public $name, $slug, $description, $subjectId, $hideImage, $menu, $status, $image, $icon, $logo, $color, $order, $view;
    public $metaKeyword, $metaDescription, $seoImage, $editImage, $editSeoImage;
    public $imagePath, $seoImagePath;
    public $search = '';

    public function mount($id)
    {
        if ($id) {
            $this->editSubject($id);
        }
    }

    public function render()
    {
        return view('livewire.backend.subjects.edit-subject');
    }

    public function updatedName()
    {
        $this->slug = SlugService::createSlug(Subject::class, 'slug', $this->name);
    }

    private function editSubject($id)
    {
        $subject = Subject::find($id);
        $this->subjectId = $subject->id;
        $this->name = $subject->name;
        $this->slug = $subject->slug;
        $this->description = $subject->description;
        $this->menu = $subject->menu;

        $this->status = $subject->status;
        $this->icon = $subject->icon;
        $this->view = $subject->view;

        $this->color = $subject->color;
        $this->order = $subject->order;
        $this->subjectType = $subject->subject_type;
        $this->subjectPermalink = $subject->subject_permalink;

        $this->subjectShortTitle = $subject->subject_short_title;
        $this->commentStatus = $subject->comment_status;
        $this->createdBy = $subject->created_by;
        $this->updatedBy = $subject->updated_by;

        $this->metaKeyword = $subject->meta_keyword;
        $this->metaDescription = $subject->meta_description;
        
        $this->image = $subject->image;
        $this->imagePath = $this->image ? Storage::url($this->image) : '#';
        $this->seoImage = $subject->seo_image;
        $this->seoImagePath = $this->seoImage ? Storage::url($this->seoImage) : '#';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|string',
            'slug' => 'required|unique:subjects|string',
        ]);
        if ($this->editImage) {
    		$image = $this->uploadImage($this->editImage);
            $this->deleteImage($this->image);
    	}
        if ($this->editSeoImage) {
    		$seoImage = $this->uploadImage($this->editSeoImage);
            $this->deleteImage($this->seoImage);
    	}
        $userName = Auth::user()->username;  
        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'menu' => $this->menu,
            'status' => $this->status ? 'active' : 'inactive',
            'icon' => $this->icon,
            'view' => $this->view,
            'color' => $this->color,
            'order' => $this->order,
            'meta_keyword' => $this->metaKeyword,
            'meta_description' => $this->metaDescription,
            'image' => $image ?? null,
            'seo_image' => $seoImage ?? null,
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
        );
        
        $subject = Subject::updateOrCreate(['id' => $this->subjectId], $data);
        session()->flash('success', 'Subject Updated Successfully. 😁');
        return redirect()->route('backend.subject.edit',[$userName, $subject->id]);
    }
}
