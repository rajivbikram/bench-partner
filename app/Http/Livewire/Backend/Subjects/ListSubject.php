<?php

namespace App\Http\Livewire\Backend\Subjects;

use App\Models\Subject;
use Livewire\Component;
use Livewire\WithPagination;

class ListSubject extends Component
{
	use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $updateBy = null, $createdBy = null, $deleteId;
    public $search = '';

    protected $listeners = ['deleteConfirmed' => 'delete'];

    public function render()
    {
        $subjects = Subject::search($this->search)
        		->latest()
        		->paginate(10);

        return view('livewire.backend.subjects.list-subject',
            ['subjects' => $subjects,
        ]);
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
        $this->dispatchBrowserEvent('delete-conformation');
    }

    public function delete()
    {
        try {
        	$category = Subject::find($this->deleteId);
            if (isset($category)) {
		        $category->delete();
		        session()->flash('success', 'Subject Deleted Successfully.');
            }else{
                session()->flash('error', 'Id could not be obtained!');
            }
            
         } catch (\Exception $e) {
            return $e->getMessage();
            session()->flash('error', 'Exception : ' . $e);
        }
    }
}
