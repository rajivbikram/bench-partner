<?php

namespace App\Http\Livewire\Backend\Role;

use App\Models\Role;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Livewire\Component;
use Livewire\WithPagination;

class RoleComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $name, $slug, $description, $roleId, $deleteId, $isUpdate = 0;
    public $search = '';


    public function render()
    {
        $roles = Role::search($this->search)
                ->latest()
                ->simplePaginate(10);
        return view('livewire.backend.role.index',compact('roles'))
                ->extends('layouts.frontend.dashboard.app')
                ->section('content');
    }

    public function paginationView()
    {
        //return view('layouts.backend.pagination');
    }

    public function create()
    {
        $this->resetInputFields();
        $this->isUpdate = false;
    }

    private function resetInputFields()
    {
        $this->name = '';
        $this->description = '';
    }

    public function updatedName()
    {
        $this->slug = SlugService::createSlug(Role::class, 'slug', $this->name);
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|string',
        ]);
        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description
        );
        $role = Role::updateOrCreate(['id' => $this->roleId],$data);
        session()->flash('success', $this->roleId ? 'Role Updated Successfully. 😁' : 'Role Created Successfully. 😁');
        $this->resetInputFields();
        $this->emit('closeModel');
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $this->roleId = $id;
        $this->name = $role->name;
        $this->slug = $role->slug;
        $this->description = $role->description;
        $this->isUpdate = true;
    }

    public function deleteId($id)
    {
        $this->deleteId = $id;
        $this->dispatchBrowserEvent('delete-conformation');
    }

    public function delete()
    {
        Role::find($this->deleteId)->delete();
        session()->flash('success', 'Role Deleted Successfully.');
        $this->emit('hideDeleteModel');
    }

}
