<?php

namespace App\Http\Livewire\Frontend\Subjects;

use App\Models\Subject;
use Livewire\Component;

class SubjectDetails extends Component
{
    public $subject;

    public function mount($subject)
    {
        $this->subject = Subject::with('chapters','user','posts')
                        ->where('slug', $subject)
                        ->first();
        if (!$this->subject) {
            abort(404);
        }
    }

    public function render()
    {
        return view('livewire.frontend.subject.details');
    }
}