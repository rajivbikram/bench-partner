<?php

namespace App\Http\Livewire\Frontend\Subjects;

use App\Models\Subject;
use Livewire\Component;
use Livewire\WithPagination;

class SubjectList extends Component
{
	use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '', $subjects;

    public function render()
    {
    	$this->subjects = Subject::all();

        return view('livewire.frontend.subject.index');
    }
}