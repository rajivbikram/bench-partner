<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Subject;
use App\Models\Chapter;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;
use Livewire\Component;

class PostComponent extends Component
{

	use WithFileUploads;

    public $postTitle, $postChapter, $postContent, $featuredImage, $hideFeaturedImage, $isSticky, $postStatus, $postType, $postPermalink, $postLike, $postShortTitle, $commentStatus, $createdBy, $updatedBy, $createdAt, $updatedAt;

    public $subjects, $subjectName, $subjectId, $subjectSlug;
    public $chapters, $chapterName, $chapterId;

    public $metaKeyword, $metaDescription, $seoImage, $postFaq, $faqQuestion, $faqAnswer, $seoCreatedAt, $seoUpdatedAt;
    public $faqRow = 1, $faqInputs = [];
    public $tabDetails = null, $tabSeo = null, $tabImages = null, $tabSubject = null;
    public $postSlug, $postId, $post;
    public $previousPost, $nextPost;

    public $likeCount = 0, $dislikeCount = 0, $thankCount = 0;
    
    public function mount($post)
    {
        $post = Post::with('subject','chapter','user')
                    ->where('post_slug', $post)
                    ->first();
        
        if (!$post) {
            abort(404);
        }
        $this->viewPost($post);
        
    }
    public function getChapters($id)
    {
        $chapters = Chapter::where('subject_id', $id)->get();
        $this->subjectId = $id;
        $this->chapters = $chapters;
    }

    public function render()
    {
        return view('livewire.frontend.post.index');
    }

    private function viewPost($post)
    {
        $this->postId = $post->id;
        $this->postTitle = $post->post_title;
        $this->postSlug = $post->post_slug;
        $this->postContent = $post->post_content;
        $this->likeCount = $post->post_like;
        $this->dislikeCount = $post->post_dislike;
        $this->thankCount = $post->post_thank;

        if ($post->subject) {
            $this->subjectId = $post->subject->id;
            $this->subjectName = $post->subject->name;
            $this->subjectSlug = $post->subject->slug;
        }

        if ($post->chapter) {
            $this->chapterId = $post->chapter->id;
        }

        if ($post->user) {
            $this->createdBy = $post->user->name;
            $this->updatedBy = $post->user->name;
        }
        
        $this->chapters = Chapter::with('posts')
                    ->where('subject_id', $this->subjectId)
                    ->get();

        $this->featuredImage = $post->featured_image;
        $this->hideFeaturedImage = $post->hide_featured_image;

        $this->isSticky = $post->is_sticky;
        $this->postStatus = $post->post_status;
        $this->postType = $post->post_type;
        $this->postPermalink = $post->post_permalink;

        $this->postShortTitle = $post->post_short_title;
        $this->commentStatus = $post->comment_status;
        $this->createdAt = $post->created_at->toFormattedDateString();
        $this->updatedAt = $post->updated_at->toFormattedDateString();
        
        $this->metaKeyword = $post->meta_keyword;
        $this->metaDescription = $post->meta_description;
        $this->seoImage = $post->seo_image;
        $this->seoCreatedAt = $post->created_at->toAtomString();
        $this->seoUpdatedAt = $post->updated_at->toAtomString();

        // Previous Post
        $this->previousPost = Post::where('id', '<', $post->id)->first();

        // Pext Post
        $this->nextPost = Post::where('id', '>', $post->id)->first();
        
    }

    public function like()
    {
        $like = Post::find($this->postId);
        $like->post_like = $like->post_like + 1;
        $like->save();
        $this->likeCount = $like->post_like;
        
    }

    public function dislike()
    {
        $like = Post::find($this->postId);
        $like->post_dislike = $like->post_dislike + 1;
        $like->save();
        $this->dislikeCount = $like->post_dislike;
    }

    public function thanks()
    {
        $thank = Post::find($this->postId);
        $thank->post_thank = $thank->post_thank + 1;
        $thank->save();
        $this->thankCount = $thank->post_thank;
    }
}