<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Subject;
use App\Models\User;
use App\Models\Chapter;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    use HasFactory, Sluggable;
    //const DIRECRORY = date('Y/m');

    public function sluggable()
    {
        return [
            'post_slug' => [
                'source' => 'post_title'
            ]
        ];
    }

    protected $fillable = [
        'post_title',
        'post_slug',
        'subject_id',
        'chapter_id',
        'post_content',
        'hide_featured_image',
        'is_sticky',
        'post_status',
        'post_type',
        'post_permalink',
        'post_short_title',
        'comment_status',
        'post_like',
        'post_view',
        'post_thank',
        'created_by',
        'updated_by',
        'seo_image',
        'meta_keyword',
        'meta_description',
    ];

    public static function imagePath() {
        return public_path(self::DIRECRORY);
    }

    public function getImagePathAttribute()
    {
        return url(self::DIRECRORY . '/' . $this->image);
    }

    public static function active()
    {
        return self::where('post_status', 'publish');
    }

    public static function scopeAutherPost($query)
    {
        if(!(auth()->check() && auth()->user()->isAdmin())){
            return $query->where('created_by', auth()->id());
        }
        
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'created_by', 'id');
    }

    public static function scopeSearch($query, $search)
    {
        return empty($search) ? $query
            : $query->where('id', 'like', '%'.$search.'%')
                ->orWhere('post_title', 'like', '%'.$search.'%')
                ->orWhere('post_short_title', 'like', '%'.$search.'%');
    }

    public function getPostStatusBadgeAttribute()
    {
        $badges = [
            'draft' => 'info',
            'publish' => 'success',
            'pending' => 'warning',
            'trash' => 'danger',
        ];

        return $badges[$this->post_status];
    }
}
