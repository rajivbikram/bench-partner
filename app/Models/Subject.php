<?php

namespace App\Models;

use App\Models\Chapter;
use App\Models\Post;
use App\Models\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Subject extends Model
{
    use HasFactory, Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $fillable = [
        'name',
        'slug',
        'description',
        'view',
        'image',
        'parent_id',
        'menu',
        'status',
        'icon',
        'order',
        'color',
        'menu',
        'logo',
        'meta_keyword',
        'meta_description',
        'seo_image',
        'created_by',
        'updated_by',
    ];

    public static function active()
    {
        return self::where('status', 'active')->orderBy('name','ASC');;
    }

    public static function search($search)
    {
        return empty($search) ? static::query()
            : static::query()->where('id', 'like', '%'.$search.'%')
                ->orWhere('name', 'like', '%'.$search.'%');
    }

    public function getImagePathAttribute()
    {
        return $this->image?Storage::url($this->image):'#';
    }

    public function user()
    {
        return $this->belongsTo(User::class,'created_by', 'id');
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class, 'subject_id')
            ->orderBy('created_at','ASC');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'subject_id')
            ->orderBy('created_at','ASC');
    }
}
