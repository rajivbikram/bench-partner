<?php

namespace App\Models;

use App\Models\SeoData;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoData extends Model
{
    use HasFactory;

    protected $fillable = [
        'table',
        'seo_id',
        'description',
        'type',
        'value',
    ];

}
