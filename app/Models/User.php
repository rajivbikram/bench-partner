<?php

namespace App\Models;

use App\Models\Role;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'profile_image',
        'cover_image',
        'provider_id', 
        'provider',
        'access_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function hasRole( ... $roles )
    {
        foreach ($roles as $role) {
            if ($this->role->slug == $role) {
                return true;
            }
        }
        return false;
    }

    public function isAdmin(){
        return $this->role->slug=='admin'?true:false;
    }

    public function isEditor(){
        return $this->role->slug=='editor'?true:false;
    }

    public function isLecturer(){
        return $this->role->slug=='lecturer'?true:false;
    }

    public function isUser(){
        return $this->role->slug=='user'?true:false;
    }

    // public function getCreatedAtAttribute($date)
    // {
    //     return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F d, Y');
    // }
}
