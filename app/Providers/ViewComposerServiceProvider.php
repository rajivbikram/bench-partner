<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class ViewComposerServiceProvider extends ServiceProvider 
{
    public function __construct()
    {
        //
    }
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() 
	{
		

        view()->composer('*', function ($view)
            {
                if (Auth::check()) {
                   $username = auth()->user()->username;
                   $view->with('username', $username);
                }

				$bodyClass = '';
				if (Route::currentRouteName() == 'frontend.subject.details' || Route::currentRouteName() == 'frontend.post.details') {
					$bodyClass = 'class=is-sidebar';
				}
				$view->with('bodyClass', $bodyClass);
                
            });
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
