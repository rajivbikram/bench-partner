<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Trait UploadAble
 * @package App\Traits
 */
trait UploadImage
{
    /**
     * @param UploadedFile $file
     * @param null $folder
     * @param string $disk
     * @return false|string
     */

    public function uploadImage(UploadedFile $file, $folder = null, $disk = 'public')
    {
        $folder = date('Y/m');
        $extension = $file->extension();
        $name = $file->getClientOriginalName();
        $nameWithoutExtension = pathinfo($name,PATHINFO_FILENAME);
        $fullName = Str::slug($nameWithoutExtension).'.'.$extension;
        $uploadedFile = $file->storeAs($folder,$fullName,$disk='public');
        return $uploadedFile;
    }

    /**
     * @param null $path
     * @param string $disk
     */
    public function deleteImage($path = null, $disk = 'public')
    {
        Storage::disk($disk)->delete($path);
    }
}