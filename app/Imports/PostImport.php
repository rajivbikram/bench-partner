<?php

namespace App\Imports;

use App\Models\Post;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PostImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Post([
            'post_title' => $row['title'],
            'post_slug' => $row['slug'],
            'post_content' => $row['content'],
            'post_status' => 'publish',
            'post_type' => 'post',
            'post_permalink' => $row['permalink'],
            'post_short_title' => $row['short'],
            'post_view' => $row['view'] ?? 0,
            'created_by' => $row['created'],
            'updated_by' => $row['created'],
            'meta_keyword' => $row['title'],
            'meta_description' => $row['title']
        ]);
    }

    public function rules(): array
    {
        return [
            'post_title' => 'required',
            'post_slug' => 'required|unique:posts',
        ];
    }

}
