<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImportFile
{
    /**
     * @param UploadedFile $file
     * @param null $folder
     * @param string $disk
     * @return false|string
     */

    public function uploadOne(UploadedFile $file, $folder = 'imports', $disk = 'public')
    {
        $fileNameWithExtension = $file->getClientOriginalName();
        $fileNameWithoutExtension = str_replace('.', ' ', $fileNameWithExtension);
        $originalName = explode(' ',  $fileNameWithoutExtension)[1];

        $uploadedFile = $file->storeAs($folder,$fileNameWithExtension,$disk='public');
        return $uploadedFile;
    }

    /**
     * @param null $path
     * @param string $disk
     */
    public function deleteOne($path = null, $disk = 'public')
    {
        Storage::disk($disk)->delete($path);
    }

}