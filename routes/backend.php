<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Backend\PostController;
use App\Http\Livewire\Backend\Subjects\ListSubject;
use App\Http\Livewire\Backend\Subjects\AddSubject;
use App\Http\Livewire\Backend\Subjects\EditSubject;
use App\Http\Livewire\Backend\DashboardComponent;
use App\Http\Livewire\Backend\Posts\AddPost;
use App\Http\Livewire\Backend\Posts\EditPost;
use App\Http\Livewire\Backend\Posts\ImportPost;
use App\Http\Livewire\Backend\Posts\ListPost;
use App\Http\Livewire\Backend\Role\RoleComponent;
use App\Http\Livewire\Backend\Chapter\ChapterComponent;
use App\Http\Livewire\Backend\Setting\SettingComponent;
use App\Http\Livewire\Backend\Setting\ProfileComponent;
use App\Http\Livewire\Backend\Users\ListUsers;
use Illuminate\Support\Facades\Route;


Route::get('signup', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('signup', [RegisterController::class, 'register'])->name('register');

Route::group(['prefix' => '{username}','middleware' => ['web','auth']],function(){
	
	Route::get('dashboard', DashboardComponent::class)->name('backend.dashboard');

	Route::get('roles', RoleComponent::class)->name('backend.role')->middleware('role:admin,editor');
	
	Route::get('subjects', ListSubject::class)->name('backend.subject.index');
	Route::get('subjects/create', AddSubject::class)->name('backend.subject.create');
	Route::get('subjects/{id}/edit', EditSubject::class)->name('backend.subject.edit');

	Route::get('chapters', ChapterComponent::class)->name('backend.chapter');

	Route::get('settings', SettingComponent::class)->name('backend.setting');
	Route::get('profile', ProfileComponent::class)->name('backend.profile');

	Route::get('posts', ListPost::class)->name('backend.post.index');
	Route::get('posts/create', AddPost::class)->name('backend.post.create');
	Route::get('posts/{post}/edit', EditPost::class)->name('backend.post.edit');
	
	Route::get('posts/import', ImportPost::class)->name('backend.post.import');

	Route::get('users', ListUsers::class)->name('backend.user.index');

});