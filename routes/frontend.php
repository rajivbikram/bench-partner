<?php

use App\Http\Livewire\Frontend\HomeComponent;
use App\Http\Livewire\Frontend\PostComponent;
use App\Http\Livewire\Frontend\Subjects\SubjectList;
use App\Http\Livewire\Frontend\Subjects\SubjectDetails;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

Route::get('/', HomeComponent::class)->name('home');

Auth::routes();
Route::get('oauth/{driver}', [LoginController::class, 'redirectToProvider'])->name('frontend.social.oauth');
Route::get('oauth/{driver}/callback', [LoginController::class, 'handleProviderCallback'])->name('frontend.social.callback');

Route::get('subjects', SubjectList::class)->name('frontend.subject.index');
Route::get('subject/{subject}', SubjectDetails::class)->name('frontend.subject.details');
Route::get('{post}', PostComponent::class)->name('frontend.post.details');