const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sourceMaps();


mix.styles([
    'public/frontend/css/style.css',
    'public/frontend/vendor/simplebar/dist/simplebar.min.css'
], 'public/css/all.css');


mix.scripts([
    'public/frontend/vendor/bootstrap/dist/js/bootstrap.bundle.min.js',
    'public/frontend/vendor/simplebar/dist/simplebar.min.js',
    'public/frontend/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js',
    'public/frontend/js/theme.min.js'
], 'public/js/all.js');